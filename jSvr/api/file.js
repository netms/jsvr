const fs = require("fs");

async function get (param, cfg, ctx, next) {
	const FILE = this.get("file"),
		  id   = ctx.query.d || ctx.query.k || ctx.query.id || "";

	try {
		if (!FILE || !id) throw -3;

		let data = this.File_Load(param, ctx),
			rs   = await FILE.Info(id, data.type, data.key);

		if (!rs) throw 404;

		ctx.set(rs.head);
		ctx.body = fs.createReadStream(rs.path);
	}
	catch (e) {
		throw 404;
	}
}

async function post (param, cfg, ctx, next) {
	const FILE = this.get("file"),
		  file = ctx.req.files || [],
		  done = d => {
			  if (d.length < 1) throw ctx.err(10, "上传失败！");

			  ctx.body = ctx.err(0, d);
		  };

	let data = this.File_Upload(param, ctx);

	if (!FILE || file.length < 1 || !data.id || !data.type) throw -3;

	let rs = await FILE.Upload(data.id, data.type, file),
		id = [];

	for (let i = 0, d; i < rs.length; i++) {
		d = rs[i];
		if (!d) throw 3;

		if (!d.id) d.url = "";
		else id.push(d.id);
	}

	if (data.callback) data.callback(done, rs, id);
	else done(rs);
}

exports = module.exports = {get, post};