const chkMPCode = (data, key) => {
		  return jBD.Security.MD5(data.phone + key + data.type) === data.code;
	  },
	  chkQRCode = (data, key) => {
		  return jBD.Security.MD5(key + data.code) === data.key;
	  };

async function gv_get (param, cfg, ctx, next) {
	const CHK  = this.get("gvchk"),
		  req  = ctx.request.query,
		  chk  = ctx.chk,
		  data = {
			  type: String(req.type || ""),
			  key:  String(req.key || "")
		  };

	try {
		chk.Has(data, "type", CHK.TYPE || []);
		chk.Value(data, "key");

		if (!data.type) throw 404;
		if (!data.key) throw -3;

		let rs;

		if (this.Code_GV_Get) rs = await this.Code_GV_Get(data, ctx);
		else rs = await CHK.Code(data.type, data.key);

		ctx.type = "svg";
		ctx.body = rs.data;
	}
	catch (e) {
		throw 404;
	}
}

async function gv_check_post (param, cfg, ctx, next) {
	const CHK  = this.get("gvchk"),
		  req  = ctx.request.body,
		  chk  = ctx.chk,
		  data = {
			  type: String(req.type || ""),
			  key:  String(req.key || ""),
			  code: String(req.code || "")
		  };

	chk.Has(data, "type", CHK.TYPE || []);
	chk.Value(data, "key");
	chk.Value(data, "code");

	if (!data.type) throw 404;
	if (!data.key || !data.code) throw -3;

	let rs;

	if (this.Code_GV_Check) rs = this.Code_GV_Check(data, ctx);
	else rs = CHK.Check(data.type, data.key);

	if (rs.code !== data.code) throw ctx.err(11, "验证码不正确");

	ctx.body = ctx.err(0, "验证码正确");
}

async function mp_get_post (param, cfg, ctx, next) {
	const CHK  = this.get("mpchk") || this.get("chk"),
		  req  = ctx.request.body,
		  chk  = ctx.chk,
		  data = {
			  code:  String(req.code || ""),
			  type:  String(req.type || ""),
			  phone: String(req.phone || "")
		  };

	chk.Has(data, "type", CHK.TYPE || []);
	chk.Mobile(data, "phone");

	if (!data.type) throw 404;
	if (!data.phone) throw -3;

	let rs;

	if (this.Code_MP_Get) rs = await this.Code_MP_Get(data, ctx);
	else {
		if (!chkMPCode(data, CHK.key)) throw -3;

		rs = await CHK.Code(data.type, data.phone);
	}

	if (rs.code) throw ctx.err(rs.code, "", {timeout: rs.timeout});

	ctx.body = ctx.err(0, {timeout: rs.timeout});
}

async function mp_check_post (param, cfg, ctx, next) {
	const CHK  = this.get("mpchk") || this.get("chk"),
		  req  = ctx.request.body,
		  chk  = ctx.chk,
		  data = {
			  type:  String(req.type || ""),
			  phone: String(req.phone || ""),
			  code:  String(req.code || "")
		  };

	chk.Has(data, "type", CHK.TYPE || []);
	chk.Mobile(data, "phone");
	chk.Value(data, "code");

	if (!data.type) throw 404;
	if (!data.phone || !data.code) throw -3;

	let rs = CHK.Check(data.type, data.phone);

	if (rs.code !== data.code) throw ctx.err(11, "验证码不正确");

	ctx.body = ctx.err(0, "验证码正确");
}

async function qr_get_post (param, cfg, ctx, next) {
	const qr   = require("qr-image"),
		  req  = ctx.request.body,
		  chk  = ctx.chk,
		  data = {
			  key:  String(req.key || ""),
			  code: String(req.code || "")
		  };

	try {
		chk.Value(data, "code");

		if (!data.code) throw -3;
		if (!chkQRCode(data, "qr")) throw -3;

		ctx.type = "svg";
		ctx.body = qr.imageSync(data.code, {type: "svg"});
	}
	catch (e) {
		ctx.status = 404;
	}
}

exports = module.exports = {gv_get, gv_check_post, mp_get_post, mp_check_post, qr_get_post};