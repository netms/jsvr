async function payed (param, cfg, ctx, next) {
	const PAY = this.get("pay");

	try {
		let result = await PAY.Payed(param.key, ctx.request.rawBody);

		ctx.status = 200;
		ctx.body = result;
	}
	catch (e) {
		throw 404;
	}
}

async function refunded (param, cfg, ctx, next) {
	const PAY = this.get("pay");

	try {
		let result = await PAY.Refunded(param.key, ctx.request.rawBody);

		ctx.status = 200;
		ctx.body = result;
	}
	catch (e) {
		throw 404;
	}
}

async function prepay (param, cfg, ctx, next) {
	if (!this.Pay_Prepay) throw 404;

	let result = await this.Pay_Prepay(param.key, ctx.request.body);

	ctx.body = ctx.err(0, result);
}

async function refund (param, cfg, ctx, next) {
	if (!this.Pay_Refund) throw 404;

	let result = await this.Pay_Refund(param.key, ctx.request.body);

	ctx.body = ctx.err(0, result);
}

exports = module.exports = {prepay, refund, payed, refunded};