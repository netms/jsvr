/**
 * ==========================================
 * Name:           jSvr‘s 基础操作
 * Author:         Buddy-Deus
 * CreTime:        2018-07-11
 * Description:    jSvr's Tools for JavaScript
 * Log:
 * 2018-07-11    初始化
 * ==========================================
 */
(TSvr => {
	const fs      = require("fs"),
		  path    = require("path"),
		  redis   = require("ioredis"),
		  routers = require("koa-router"),
		  CHK     = {
			  Auth:     function (ctx, auth) {
				  let method = ctx.method.toLowerCase(),
					  req    = ctx.request,
					  header = req.headers,
					  key    = header["x-secret-key"] || header["x-auth-key"] || "",
					  sign   = header["x-secret-sign"] || header["x-auth-sign"] || "";

				  req = method === "get" ? req.query : req.body;

				  if (!auth || !key || !sign) throw 404;
				  if (!auth.Check(key, sign, req)) throw 404;

				  return true;
			  },
			  Value:    function (data, key, callback) {
				  try {
					  if (!jBD.isObject(data)) throw 0;

					  let value = data[key];

					  if (!value && value !== 0) throw 0;

					  if (callback) data[key] = callback(value);

					  return true;
				  }
				  catch (e) {
					  delete data[key];
				  }

				  return false;
			  },
			  Number:   function (data, key, opt) {
				  return this.Value(data, key, value => {
					  value = Number(value);
					  if (isNaN(value)) throw 0;
					  if (opt && !jBD.isNumer(value, opt)) throw 0;

					  return value;
				  });
			  },
			  Has:      function (data, key, list) {
				  return this.Value(data, key, value => {
					  if (!jBD.has(value, list)) throw 0;

					  return value;
				  });
			  },
			  Token:    function (data, key, len) {
				  if (arguments.length == 1) return jBD.isString(data) ? data.length == 32 : false;
				  else {
					  return this.Value(data, key, value => {
						  if (value.length != (len || 32)) throw 0;

						  return value;
					  });
				  }
			  },
			  Mobile:   function (data, key) {
				  return this.Value(data, key, value => {
					  if (!jBD.Check.Mobile(value)) throw 0;

					  return value;
				  });
			  },
			  Account:  function (data, key, min, max) {
				  return this.Value(data, key, value => {
					  if (!jBD.Check.UID(value, min, max)) throw 0;

					  return value;
				  });
			  },
			  Password: function (data, key) {
				  return this.Value(data, key, value => {
					  if (value.length != 32) throw 0;

					  return value;
				  });
			  },
			  UID:      function (data, key) {
				  return this.Value(data, key, value => {
					  if (!jBD.Check.UID(value)) throw 0;

					  return value;
				  });
			  },
			  PWD:      function (data, key) {
				  return this.Value(data, key, value => {
					  if (value.length != 32) throw 0;

					  return value;
				  });
			  },
			  ObjectID: function (data, key) {
				  if (arguments.length == 1) return jBD.isString(data) ? data.length == 24 : false;
				  else {
					  return this.Value(data, key, value => {
						  if (value.length != 24) throw 0;

						  return value;
					  });
				  }
			  },
			  OArray:   function (data, key, DB) {
				  return this.Value(data, key, value => {
					  value = value.split("|");

					  let i = 0;

					  while (i < value.length) {
						  if (value[i].length != 24) value.splice(i, 1);
						  else {
							  if (DB) value[i] = DB.ObjectID(value[i]);
							  i++;
						  }
					  }

					  return value;
				  });
			  },
			  SArray:   function (data, key) {
				  return this.Value(data, key, value => {
					  return value.split("|");
				  });
			  },
			  NArray:   function (data, key) {
				  return this.Value(data, key, value => {
					  value = value.split("|");

					  jBD.each(value, (d, i) => {
						  d = Number(d);
						  if (isNaN(d)) throw 0;
						  value[i] = d;
					  });

					  return value;
				  });
			  },
			  Between:  function (data, key) {
				  return this.Value(data, key, value => {
					  value = value.split("|").slice(0, 2);

					  jBD.each(value, (d, i) => {
						  d = Number(d);
						  if (isNaN(d)) throw 0;

						  value[i] = d;
					  });
					  if (value.length < 2) value.unshift(0);

					  return value;
				  });
			  },
			  File:     function (data, key, type, field) {
				  return this.Value(data, key, value => {
					  let r = [];

					  for (let i = 0, d; i < value.length && (d = value[i]); i++) {
						  if (!type || d.fieldname !== field) fs.unlink(d.path);
						  else r.push({
							  name: d.originalname,
							  path: d.path,
							  size: d.size,
							  md5:  d.filename
						  });
					  }

					  if (r.length < 0) throw 0;

					  return r;
				  });
			  },
			  Object:   function (data, key, callback) {
				  return this.Value(data, key, value => {
					  if (!jBD.isObject(value)) throw 0;
					  if (callback && callback(value) === false) throw 0;
					  return value;
				  });
			  },
			  Array:    function (data, key, callback) {
				  return this.Value(data, key, value => {
					  if (!jBD.isArray(value)) throw 0;
					  if (callback && callback(value) === false) throw 0;
					  return value;
				  });
			  },
			  Date:     function (data, key, callback) {
				  return this.Value(data, key, value => {
					  if (!jBD.isDate(value)) throw 0;
					  if (callback && callback(value) === false) throw 0;
					  return value;
				  });
			  },
			  Mail:     function (data, key, callback) {
				  return this.Value(data, key, value => {
					  if (!jBD.Check.Mail(value)) throw 0;
					  if (callback && callback(value) === false) throw 0;
					  return value;
				  });
			  }
		  },
		  ERR     = TSvr.ERROR;

	const init_Error         = (e, ctx, core) => {
			  switch (jBD.type(e)) {
				  case "number":
					  if (e >= 100 && e <= 600) {
						  if (core) core.call("error", ctx.err(e, "http error code"));
						  ctx.status = e;
					  }
					  else {
						  e = ctx.err(e || -4);
						  if (core) core.call("error", e);
						  ctx.body = e;
					  }
					  break;
				  default:
					  e = ctx.err(-4);
				  case "object":
					  if (!jBD.isNumber(e.errorCode)) e = ctx.err(jBD.Conver.toInteger(e.message || e, -4));
					  if (core) core.call("error", e);
					  ctx.body = e;
					  break;
			  }
		  },
		  init_Koa           = (err, chk, root, callback) => {
			  const koa = require("koa"),
					app = new koa();

			  app.context.err = err;
			  app.context.chk = chk;

			  // app.use(require("koa-cors")());
			  app.use(require("koa-body")({
				  jsonLimit: "100mb",
				  formLimit: "100mb",
				  textLimit: "100mb",
				  multipart: false
			  }));

			  if (callback) callback(app);

			  if (root) app.use(require("koa-static")(root));

			  return app;
		  },
		  init_Server        = (app, core, sys) => {
			  const http = require("http");

			  http.globalAgent.maxSockets = sys.max;

			  app.listen(sys.port, sys.host, () => {
				  console.info("========================================");
				  console.info("HTTP Listening: " + sys.host + ":" + sys.port);
				  console.info("****************************************\n");

				  if (core && sys.start) core.Start();
			  });
		  },
		  init_Router_jBD    = (app, root) => {
			  const router    = routers(),
					files     = {},
					urlencode = value => {
						value = encodeURIComponent(value + "")
							.replace(/!/g, "%21")
							.replace(/'/g, "%27")
							.replace(/\(/g, "%28")
							.replace(/\)/g, "%29")
							.replace(/\*/g, "%2A")
							.replace(/%20/g, "+");

						return value;
					},
					state     = (o, p) => {
						return {
							"Content-Type":        "application/javascript;charset=utf8",
							"Coneten-Length":      o.size,
							"Content-Disposition": "attachment;filename=" + urlencode(path.basename(p))
						};
					},
					each      = (root, url) => {
						let ls = fs.readdirSync(root);

						for (let i = 0, p, o, u; i < ls.length; i++) {
							u = url + "/" + ls[i];
							p = path.join(root, ls[i]);
							o = fs.statSync(p);

							if (o.isFile()) files[u.substr(1)] = state(o, p);
							else if (ls[i] !== "node") each(p, u);
						}
					};

			  each(root, "");

			  router
				  .prefix("/inc/jBD")
				  .get("/:file*", async function (ctx, next) {
					  let file = ctx.params.file,
						  head = files[file];

					  if (!head) await next();
					  else {
						  ctx.set(head);
						  ctx.body = fs.createReadStream(path.join(root, file));
					  }
				  });

			  app.use(router.routes(), router.allowedMethods());
		  },
		  init_Router_Page   = (app, core, url, cfg, page, view, vroot, mode) => {
			  if (!page.__child && !view.__child) return;

			  const views  = require("koa-views"),
					router = routers(),
					ip     = jBD.Net.httpClientIP;

			  const cpage = async function (next, func, key, method, cfg, core) {
						if (!func) await next();
						else {
							if (mode === "debug") console.info(`[${method}] ${ip(this.req)} ${key}`);

							try {
								func = func[method] || func["all"];
								if (!func) throw 0;

								await func.call(core, cfg, key, this, next);
							}
							catch (e) {
								core.call("error", e);
								this.status = 404;
							}
						}
					},
					cview = async function (next, type, key, method) {
						if (type !== 1) await next();
						else {
							if (mode === "debug") console.info(`[${method}] ${ip(this.req)} ${key}`);

							await this.render(key, method === "post" ? this.body : this.query);
						}
					},
					cfunc = (router, type, url, page, list, cfg) => {
						let fun = router[type ? "get" : "all"],
							uri = type ? "/" : "/:key";

						if (url !== "/") uri = url + uri;

						fun.call(router, uri, async function (ctx, next) {
							let key    = type ? "index" : ctx.params.key,
								method = ctx.method.toLowerCase(),
								func   = list[key];

							if (func && page) await cpage.call(ctx, next, func, key, method, cfg, core);
							else if (func === 1) await cview.call(ctx, next, func, key, method);
							else await next();
						});
					},
					each  = (router, url, list, cfg) => {
						let r = routers(),
							i = 0;

						jBD.each(list, (d, k) => {
							if (!d || k === "__child" || !d.__child) return;

							each(r, k, d, cfg);
							cfunc(r, "", url, true, list, cfg);
							cfunc(r, "index", url, true, list, cfg);

							i++;
						});

						if (i) router.use(`/${url}`, r.routes(), r.allowedMethods());
					};

			  if (view.__child) {
				  app.use(views(vroot, {extension: "pug"}));

				  cfunc(router, "", url, false, view, cfg);
			  }

			  if (page.__child) {
				  each(router, url, page, cfg);

				  cfunc(router, "", url, true, page, cfg);
			  }

			  cfunc(router, "index", url, page["index"], page["index"] ? page : view, cfg);

			  app.use(router.routes(), router.allowedMethods());
		  },
		  init_Router_API    = (app, core, url, cfg, api, mode) => {
			  if (!api.__child) return;

			  const err    = init_Error,
					auth   = core.get("auth"),
					router = routers();

			  router
				  .prefix(url)
				  .all("/:key*", async function (ctx, next) {
					  let ip     = jBD.Net.httpClientIP(ctx.req),
						  key    = ctx.params.key,
						  method = ctx.method.toLowerCase(),
						  keys   = key.split("/"),
						  func   = api[keys[0]];

					  if (!func) await next();
					  else {
						  try {
							  if (mode === "debug") console.info(`[${method}] ${ip} ${key}`);

							  keys.shift();
							  func = func[method] || func["all"];
							  if (!func) throw 404;
							  if (!core.IsRunning) throw 404;
							  if (auth) ctx.chk.Auth(ctx, auth);

							  await func.call(core, {ip, keys}, cfg, ctx, next);
						  }
						  catch (e) {
							  err(e, ctx, auth);
						  }
					  }
				  });

			  app.use(router.routes(), router.allowedMethods());
		  },
		  init_Router_File   = (app, core, url, cfg, file) => {
			  const err    = init_Error,
					cfile  = core.get("file"),
					router = cfile ? routers() : null;

			  if (!cfile) return;

			  let multer = require("koa-multer"),
				  api    = require("./api/file.js"),
				  // storage = multer.diskStorage({
				  //   destination: function (req, f, cb) {
				  // 	  cb(null, file.temp)
				  //   },
				  //   filename:    function (req, f, cb) {
				  // 	  cb(null, `${f.fieldname}-${Date.now()}_${f.originalname}`);
				  //   }
				  // }),
				  // upload  = multer({storage: storage}),
				  upload = multer({dest: file.temp});

			  router
				  .prefix(url)
				  .get("/:key", async function (ctx, next) {
					  let ip  = jBD.Net.httpClientIP(ctx.req),
						  key = ctx.params.key;

					  try {
						  if (!jBD.has(key, ["temp", "path"])) throw 404;
						  if (!core.IsRunning) throw -1;

						  await api["get"].call(core, {key, ip}, cfg, ctx, next);
					  }
					  catch (e) {
						  err(e, ctx, cfile);
					  }
				  })
				  .post("/:type", upload.array("file", 10), async function (ctx, next) {
					  let ip   = jBD.Net.httpClientIP(ctx.req),
						  type = ctx.params.type;

					  try {
						  if (!jBD.has(type, cfile.TYPE)) throw 404;
						  if (!core.IsRunning) throw -1;

						  await api["post"].call(core, {type, ip}, cfg, ctx, next);
					  }
					  catch (e) {
						  err(e, ctx, cfile);
					  }
				  });

			  app.use(router.routes(), router.allowedMethods());
		  },
		  init_Router_Verify = (app, core, url, cfg) => {
			  const err    = init_Error,
					cchk   = core.get("chk") || core.get("gvchk") || core.get("mpchk"),
					router = cchk ? routers() : null;

			  if (!cchk) return;

			  let api = require("./api/verify.js"),
				  chk = cfg.chk;

			  router
				  .prefix(`${url}/verify`)
				  .get("/:key", async function (ctx, next) {
					  let ip  = jBD.Net.httpClientIP(ctx.req),
						  key = ctx.params.key.toLowerCase();

					  try {
						  if (!chk[key]) throw 404;
						  if (!core.IsRunning) throw -1;

						  await api[key + "_get"].call(core, {ip, key}, cfg, ctx, next);
					  }
					  catch (e) {
						  err(e, ctx, cchk);
					  }
				  })
				  .post("/:key/:method", async function (ctx, next) {
					  let ip     = jBD.Net.httpClientIP(ctx.req),
						  key    = ctx.params.key.toLowerCase(),
						  method = ctx.params.method.toLowerCase();

					  try {
						  if (!chk[key] && key !== "qr") throw 404;
						  if (!core.IsRunning) throw -1;

						  await api[`${key}_${method}_post`].call(core, {ip, key}, cfg, ctx, next);
					  }
					  catch (e) {
						  err(e, ctx, cchk);
					  }
				  });

			  app.use(router.routes(), router.allowedMethods());
		  },
		  init_Router_Pay    = (app, core, url, cfg) => {
			  const err    = init_Error,
					cpay   = core.get("pay"),
					router = cpay ? routers() : null;

			  if (!cpay) return;

			  let api = require("./api/pay.js"),
				  pay = cfg.pay;

			  router
				  .prefix(`${url}/pay`)
				  .post("/:key/:method", async function (ctx, next) {
					  let ip     = jBD.Net.httpClientIP(ctx.req),
						  key    = ctx.params.key.toLowerCase(),
						  method = ctx.params.method.toLowerCase();

					  try {
						  if (!pay[key]) throw 404;
						  if (!core.IsRunning) throw -1;

						  await api[method].call(core, {key, ip}, cfg, ctx, next);
					  }
					  catch (e) {
						  err(e, ctx, cpay);
					  }
				  });

			  app.use(router.routes(), router.allowedMethods());
		  };

	jSvr = function (cfg, core, callback) {
		let app;

		app = init_Koa(ERR, CHK, cfg.file.root, app => {
			if (cfg.cache && cfg.cache.state) app.context.cache = new redis(cfg.cache.url);
			if (cfg.session && cfg.session.state && cfg.session.timeout) app.use(require("./lib/session.js")(cfg.session));
			if (callback) callback(app);
		});

		init_Router_jBD(app, path.join(__dirname, "../jbd"));
		init_Router_Page(app, core, cfg.sys.page, cfg.module, cfg.page, cfg.view, cfg.file.view, cfg.mode);
		init_Router_API(app, core, cfg.sys.api, cfg.module, cfg.api, cfg.mode);
		init_Router_File(app, core, cfg.sys.file, cfg.module, cfg.file.upload);

		init_Router_Verify(app, core, cfg.sys.api, cfg.module);
		init_Router_Pay(app, core, cfg.sys.api, cfg.module);

		init_Server(app, core, cfg.sys);

		return app;
	};
	jSvr.Config = function (fn, opt, plugins, callback) {
		let file  = fs.existsSync(fn) ? jBD.FS.Load(fn) : {},
			csvr  = {},
			cfile = {},
			pfn   = path.join(__dirname, "../../package.json"),
			pkg;

		const sys_SSL       = (cfg, svr, file) => {
				  if (!cfg) return;

				  svr.ssl = file.ssl = {
					  key:  "",
					  cert: ""
				  };
				  svr = svr.ssl;

				  if (fs.existsSync(cfg.key)) svr.key = cfg.key;
				  if (fs.existsSync(cfg.cert)) svr.cert = cfg.cert;
			  },
			  sys_URL       = url => {
				  if (!url || !(url = url.replace(/\\/g, "/"))) return "";

				  const SINGLE_DOT_RE = /([^/])\/[^/]+\//,
						DOUBLE_DOT_RE = /\/[^/]+\/\.\.\//;

				  url = url.replace(/\/\.\//g, "/").replace(/([^:\/])\/+\//g, "$1/");
				  while (url.match(SINGLE_DOT_RE)) url = url.replace(SINGLE_DOT_RE, "$1/");
				  while (url.match(DOUBLE_DOT_RE)) url = url.replace(DOUBLE_DOT_RE, "/");
				  while (url[0] === "/") url = url.substr(1, url.length - 1);

				  return url;
			  },
			  parse_Sys     = (cfg) => {
				  let svr = csvr.sys = {},
					  file = cfile.sys = {};

				  svr.host = file.host = jBD.isString(cfg.host) ? cfg.host : "127.0.0.1";
				  svr.port = file.port = jBD.Conver.toInteger(cfg.port, svr.ssl ? 8443 : 8080);
				  svr.max = file.max = jBD.Conver.toInteger(cfg.max, 100);

				  if (svr.host === "localhost") svr.host = file.host = "127.0.0.1";

				  sys_SSL(cfg.ssl, svr, file);

				  svr.start = file.start = cfg.start !== false;

				  svr.page = file.page = "/" + sys_URL(jBD.isString(cfg.page) ? cfg.page : "");
				  svr.api = file.api = "/" + sys_URL(jBD.isString(cfg.api) ? cfg.api : "api");
				  svr.file = file.file = "/" + sys_URL(jBD.isString(cfg.file) ? cfg.file : "file");
			  },
			  parse_Cache   = (cfg) => {
				  let svr = csvr.cache = {},
					  file = cfile.cache = {};

				  svr.state = file.state = cfg.state === true;

				  if (!svr.state) delete cfile.cache;
				  else {
					  svr.url = file.url = jBD.isString(cfg.url) ? cfg.url : "redis://127.0.0.1:6379/0";
				  }
			  },
			  parse_Session = (cfg) => {
				  let svr = csvr.session = {},
					  file = cfile.session = {};

				  svr.state = file.state = cfg.state === true;

				  if (!svr.state) delete cfile.session;
				  else {
					  svr.url = file.url = jBD.isString(cfg.url) ? cfg.url : "redis://127.0.0.1:6379/1";
					  svr.timeout = file.timeout = jBD.Conver.toInteger(cfg.timeout, 60);
				  }
			  },
			  parse_Module  = (cfg) => {
				  let svr = csvr.module = {},
					  file = cfile.module = {};

				  jBD.each(cfg, (d, k) => {
					  switch (k) {
						  case "db":
							  if (plugins.DB) {
								  svr[k] = {};
								  file[k] = {};
								  plugins.DB.Config(d, svr[k], file[k]);
							  }
							  break;
						  case "auth":
							  if (plugins.Auth) {
								  svr[k] = {};
								  file[k] = {};
								  plugins.Auth.Config(d, svr[k], file[k]);
							  }
							  break;
						  case "file":
							  if (plugins.File) {
								  svr[k] = {};
								  file[k] = {};
								  plugins.File.Config(d, svr[k], file[k], csvr.file.upload, csvr.sys.file);
							  }
							  break;
						  case "act":
							  if (plugins.Act) {
								  svr.act = {};
								  file.act = {};
								  jBD.each(d, (d, k) => {
									  svr.act[k] = {};
									  file.act[k] = {};
									  plugins.Act.Config(d, svr.act[k], file.act[k]);
								  });
							  }
							  break;
						  case "chk":
							  if (plugins.Chk) {
								  svr.chk = file.chk = {gv: null, mp: null};
								  jBD.each(d, (d, k) => {
									  svr.chk[k] = {};
									  file.chk[k] = {};
									  plugins.Chk.Config(d, svr.chk[k], file.chk[k]);
								  });
								  jBD.each(svr.chk, (d, k) => {
									  if (d) return;

									  delete svr.chk[k];
									  delete file.chk[k];
								  });
							  }
							  break;
						  case "pay":
							  if (plugins.Pay) {
								  svr[k] = {};
								  file[k] = {};
								  plugins.Pay.Config(d, svr[k], file[k]);
							  }
							  break;
						  default:
							  if (callback) callback(d, k, svr, file, opt, plugins);
							  break;
					  }
				  });

				  if (jBD.isNull(svr, {obj: true})) delete cfile.module;
			  },
			  parse_View    = (root) => {
				  let svr = csvr.view = {};

				  if (!root || !fs.existsSync(root) || !fs.statSync(root).isDirectory()) return;

				  jBD.FS.List(root, {show: "file"}, n => {
					  n = n.split(".");
					  svr[n[0]] = 1;
				  });

				  svr.__child = !jBD.isNull(svr, {obj: true});
			  },
			  parse_Page    = (cfg, root, view) => {
				  function each (cfg, svr, file, root) {
					  let r = root && fs.existsSync(root) && fs.statSync(root).isDirectory();

					  jBD.each(cfg, (d, k, t) => {
						  switch (t) {
							  case "object":
								  svr[k] = {};
								  file[k] = {};
								  each(d, svr[k], file[k], path.join(root, k));

								  if (jBD.isNull(svr[k], {obj: true})) delete svr[k];
								  else svr[k].__child = true;
								  break;
							  case "boolean":
								  file[k] = d;
								  if (!d || !r) return;

								  if (view[k]) view[k] = 2;

								  d = path.join(root, k + ".js");
								  if (fs.existsSync(d)) svr[k] = require(d);
								  break;
						  }
					  }, true);
				  }

				  let svr = csvr.page = {},
					  file = cfile.page = {};

				  each(cfg, svr, file, root);
				  svr.__child = !jBD.isNull(svr, {obj: true});

				  if (!svr.__child) delete cfile.page;
			  },
			  parse_API     = (cfg, root) => {
				  let r   = root && fs.existsSync(root) && fs.statSync(root).isDirectory(),
					  svr = csvr.api = {},
					  file = cfile.api = {};

				  jBD.each(cfg, (d, k) => {
					  file[k] = d;
					  if (!d || !r) return;

					  d = path.join(root, k + ".js");
					  if (fs.existsSync(d)) svr[k] = require(d);
				  });
				  svr.__child = !jBD.isNull(svr, {obj: true});

				  if (!svr.__child) delete cfile.api;
			  };

		try {
			opt = {
				root:   opt.root,
				api:    opt.api,
				view:   opt.view,
				page:   opt.page,
				upload: opt.upload
			};

			csvr.mode = cfile.mode = file.mode === "debug" ? "debug" : "none";
			csvr.key = cfile.key = jBD.isString(file.key) ? file.key : "jsvr";

			csvr.file = {
				root:   opt.root,
				page:   opt.page,
				view:   opt.view,
				api:    opt.api,
				upload: opt.upload
			};

			if (csvr.file.upload) {
				let upload = csvr.file.upload;

				switch (jBD.type(upload, true)) {
					case "object":
						upload = {
							temp: upload.temp || "",
							path: upload.path || ""
						};
						break;
					case "string":
						upload = {
							root: upload,
							temp: "",
							path: ""
						};
						upload.temp = path.join(upload.root, "temp");
						upload.path = path.join(upload.root, "path");
						delete upload.root;
						break;
				}

				csvr.file.upload = upload;
			}

			csvr.plugins = plugins;

			parse_Sys(jBD.isObject(file.sys) ? file.sys : {});

			parse_Cache(jBD.isObject(file.cache) ? file.cache : {});

			parse_Session(jBD.isObject(file.session) ? file.session : {});

			parse_Module(jBD.isObject(file.module) ? file.module : {});

			parse_View(csvr.file.view);

			parse_Page(jBD.isObject(file.page) ? file.page : {}, csvr.file.page, csvr.view);

			parse_API(jBD.isObject(file.api) ? file.api : {}, csvr.file.api);
		}
		finally {
			jBD.FS.Save(fn, {data: jBD.Conver.toString(cfile, 4), write: true});
		}

		return csvr;
	};
	jSvr.Depend = function (fn, cfg) {
		let pkg = require(fn) || {},
			dep = pkg.dependencies || {},
			set = key => {
				if (!dep[key]) dep[key] = "*";
			};

		try {
			if (cfg.view.__child) {
				set("koa-views");
				set("pug");
			}
			if (cfg.module) {
				if (cfg.module.file) set("koa-multer");
				if (cfg.module.db) {
					switch (cfg.module.db.mode) {
						case "mysql":
							set("mysql");
							break;
						case "mongo":
							set("mongodb");
							break;
						case "redis":
							set("ioredis");
							break;
					}
				}
				if (cfg.module.chk) {
					switch (cfg.module.chk.mode) {
						case "svg":
							set("svg-captcha");
							break;
					}
				}
			}
			if (cfg.cache && cfg.cache.state) set("ioredis");
			if (cfg.session && cfg.session.state) {
				set("ioredis");
				set("koa-session2");
			}

			pkg.dependencies = dep;
		}
		finally {
			jBD.FS.Save(fn, {data: jBD.Conver.toString(pkg, 4), write: true});
		}
	};
	jSvr.Core = function (plugins, cfg) {
		return (new jSvr.TSvr(cfg));
	};

	jSvr.TMW = jBD.TMW;
	jSvr.TSvr = jBD.TSvr;
	jSvr.ERR = ERR;
	jSvr.CHK = CHK;
})(jBD.TSvr);