"use strict";

const jBD = require("jbd");

(function (global, factory) {
	if (global.jSvr) return;

	factory.call(
		{ver: "@version"},
		global
	);
})(global, function (global) {
	let jSvr = null;

"@jSvr_Code";

	jSvr.ver = this.ver;
	global.jSvr = exports = module.exports = jSvr;

"@jSvr_Loading";
});