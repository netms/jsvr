/**
 * ==========================================
 * Name:           jSvr‘s 类操作
 * Author:         Buddy-Deus
 * CreTime:        2018-07-11
 * Description:    jSvr's Tools for JavaScript
 * Log:
 * 2018-07-11    初始化
 * ==========================================
 */
(TObject => {
	const TMW = TObject.extend({
		className: "TMW",
		create:    function () {
			this.__plugins = {};
		},
		free:      function () {
			this.__plugins = null;
		},
		get:       function (key) {
			return this.__plugins[key];
		},
		set:       function (key, value) {
			if ((this.__plugins[key] = value) && value && value.init) value.init(this);

			this.call("set", [key, value]);

			return this;
		}
	});

	jBD.TMW = TMW;
})(jBD.TObject);

(TMW => {
	const ERR  = function (code, data, msg) {
			  code = parseInt(code);

			  switch (isNaN(code) ? -1 : code) {
				  default:
					  if (msg === undefined) {
						  msg = data;
						  data = null;
					  }

					  return {
						  errorCode:  code,
						  errorMsg:   msg || "",
						  errorTime:  jBD.Conver.toString(new Date()),
						  resultData: data
					  };
				  case 0:
					  return {
						  errorCode:  0,
						  errorMsg:   "",
						  resultData: data
					  };
				  case -1:
					  return {errorCode: -1, errorTime: jBD.Conver.toString(new Date()), errorMsg: "核心服务未启动！"};
				  case -2:
					  return {errorCode: -2, errorTime: jBD.Conver.toString(new Date()), errorMsg: "数据库连接错误！"};
				  case -3:
					  return {errorCode: -3, errorTime: jBD.Conver.toString(new Date()), errorMsg: "必要参数缺失！"};
				  case -4:
					  return {errorCode: -4, errorTime: jBD.Conver.toString(new Date()), errorMsg: "API调用失败！"};
				  case 1:
					  return {errorCode: 1, errorTime: jBD.Conver.toString(new Date()), errorMsg: "登入失效！"};
				  case 2:
					  return {errorCode: 2, errorTime: jBD.Conver.toString(new Date()), errorMsg: "权限不足！"};
				  case 3:
					  return {errorCode: 3, errorTime: jBD.Conver.toString(new Date()), errorMsg: "记录不存在！"};
				  case 4:
					  return {errorCode: 4, errorTime: jBD.Conver.toString(new Date()), errorMsg: "指令操作错误！"};
			  }
		  },
		  TSvr = TMW.extend({
			  className:   "TSvr",
			  ERROR:       ERR,
			  IsRunning:   false,
			  create:      function () {
				  this.super("create", null, "TMW");

				  this.IsRunning = false;
			  },
			  free:        function () {
				  this.super("free", null, "TMW");
			  },
			  Start:       function () {
				  this.IsRunning = true;
			  },
			  Stop:        function () {
				  this.IsRunning = false;
			  },
			  File_Load:   function (data, ctx) {
				  return {
					  type: ctx.query.type || "",
					  key:  data.key
				  }
			  },
			  File_Upload: function (data, ctx) {
				  let req = ctx.request;

				  return {
					  id:   req.body.id || "",
					  type: data.type
				  };
			  }
		  });

	TSvr.ERROR = ERR;

	jBD.TSvr = TSvr;
})(jBD.TMW);