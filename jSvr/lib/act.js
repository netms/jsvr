"use strict";

const TMW = jSvr.TMW,
	  ERR = jSvr.ERR;

function MODULE (cfg, db, chk) {
	switch (cfg.mode.account) {
		case "paper":
			return new MODULE.TPaperAct(cfg, db, chk);
		default:
			return new MODULE.TLocalAct(cfg, db, chk);
	}
}

const STATE        = {
		  Inited:  -2,
		  Offline: -1,
		  Online:  1,
		  Locked:  0
	  },
	  tokenRandom  = (token, id, dt, data) => {
		  let result = jBD.Security.MD5(id + "_" + jBD.Conver.toString(dt));

		  switch (token) {
			  case "none":
				  break;
			  case "account":
				  result += data.account;
				  break;
			  case "id":
				  result += id + "";
				  break;
			  case "checkcode":
				  result += data.checkcode;
				  break;
			  case "time":
				  result += jBD.Conver.toString(new Date(), "mmddhhnnssfff");
				  break;
			  case "device":
				  result += data.device + "_" + id;
				  break;
		  }

		  return result;
	  },
	  tokenTimeout = obj => {
		  function event () {
			  tid = 0;

			  try {
				  if (info.length < 1) return;

				  let now = new Date(),
					  k, v;

				  for (k in info.actByToken) {
					  v = info.actByToken[k];

					  if (v.timeout && v.timeout <= now) obj.Logout(k);
				  }
			  }
			  finally {
				  work();
			  }
		  }

		  function work () {
			  return tid = setTimeout(event, time);
		  }

		  let tid  = 0,
			  info = obj._info,
			  time = obj.timeout;

		  return function (state) {
			  if (state !== false) work();
			  else if (tid > 0) clearTimeout(tid);
		  };
	  },
	  loginInfo    = (info, data, ip, to, token) => {
		  if (data.token) delete info.actByToken[data.token];

		  if (!data.logNow) {
			  data.logNow = {
				  type: 1,
				  ip:   ip,
				  time: new Date()
			  };
		  }

		  to = jBD.Date.IncMinute(to);

		  data.state = STATE.Online;
		  if (!data.timeout || data.timeout > to) data.timeout = to;
		  do {
			  data.token = tokenRandom(token, data._id, data.timeout, data);
		  }
		  while (info.actByToken[data.token]);

		  info.actById[data._id + ""] = info.actByToken[data.token] = data;

		  info.length++;

		  return data;
	  },
	  logInfo      = (DB, db, act) => {
		  DB.Insert(
			  db + "_Log",
			  {
				  _act: act._id,
				  type: act.logNow.type,
				  ip:   act.logNow.ip,
				  time: act.logNow.time
			  }
		  );
	  },
	  actCheck     = async (DB, db, select) => {
		  const dtd = jBD.Deferred(true);

		  try {
			  let rs = await DB.FindOne(db, select);

			  dtd.resolve(rs);
		  }
		  catch (e) {
			  dtd.reject(e);
		  }

		  return dtd.promise();
	  },
	  actUpdate    = async (DB, db, id, data) => {
		  const dtd = jBD.Deferred(true);

		  id = DB.ObjectID(id);
		  if (jBD.isNull(data, {udf: true, obj: true})) dtd.reject(ERR(-3));
		  else {
			  try {
				  let rs = await DB.Update(
					  db,
					  {_id: id},
					  {"$set": data}
				  );

				  dtd.resolve(rs);
			  }
			  catch (e) {
				  dtd.reject(e);
			  }
		  }

		  return dtd.promise();
	  };

MODULE.TAct = TMW.extend({
	className:        "TAct",
	create:           function (cfg, db) {
		this.super("create", null, "TMW");

		this._info = {
			actByToken: {},
			actById:    {},
			length:     0
		};

		this.db = cfg.db;
		this.timeout = cfg.timeout;
		this.key = cfg.key;
		this.token = cfg.token;
		this.mode = cfg.mode;

		this
			.set("db", db);

		if (cfg.timeout < 1) this.set("timeout", tokenTimeout(this)).get("timeout")();
	},
	free:             function () {
		if (this.get("timeout")) this.get("timeout")(false);

		this._info = null;

		this.super("free", null, "TMW");
	},
	INFO:             function (rs, data) {
		const result = {
			_ud:         new Date(),
			regist:      {
				ip:   rs.log[0].ip,
				time: rs.log[0].time
			},
			login:       {
				ip:   "0.0.0.0",
				time: new Date()
			},
			name:        rs.name,
			portrait:    {},
			level:       1,
			permissions: ["all"],
			type:        1,
			state:       1
		};

		jBD.each(data, (d, k) => {
			result[k] = d;
		});

		return result;
	},
	Check:            async function (data, id, n) {
		const THIS = this,
			  dtd  = jBD.Deferred(true);

		try {
			let out = {},
				rs;

			if (!id && !THIS.__checkByAccount(data, out, n)) dtd.reject(ERR(120, "账户信息错误"));
			else {
				rs = await THIS.__chkActByCheck(out, id);

				dtd.resolve(rs);
			}
		}
		catch (e) {
			dtd.reject(e);
		}

		return dtd.promise();
	},
	Token:            function (token) {
		const act = this._info.actByToken[token];

		if (act) act.timeout = jBD.Date.IncMinute(this.timeout);

		return act;
	},
	ID:               function (id) {
		return this._info.actById[id];
	},
	/**
	 * 用户登入
	 * 通过__checkByAccount设备方式
	 *
	 * @param data
	 *    @param [data.account] 用户名
	 *    @param [data.password] 密码
	 *    @param [data.paper] 密码本
	 *    @param [data.token] 设备ssh码
	 *    @param [data.device] 设备编号
	 * @returns Promise
	 */
	Login:            async function (data) {
		const THIS = this,
			  dtd  = jBD.Deferred(true);

		try {
			let rs = await THIS.Check(data, null, false);

			if (!rs) dtd.resolve(null);
			else if (rs.state === STATE.Locked) dtd.reject(ERR(112, "账号被禁用"));
			else {
				let act = await THIS.__chkActByLogin(THIS.mode, rs, data);

				act = await THIS.__doneByLogin(act, data.ip, data.device);

				dtd.resolve(act);
			}
		}
		catch (e) {
			dtd.reject(e);
		}

		return dtd.promise();
	},
	Logout:           async function (token) {
		const THIS = this,
			  dtd  = jBD.Deferred(true),
			  act  = THIS._info.actByToken[token];

		if (act) {
			let DB   = THIS.get("db"),
				data = THIS.__deleteByLogout(THIS._info, act);

			try {
				await DB.Update(
					THIS.db,
					{_id: data._id},
					{
						"$set": {
							state:   data.state,
							"log.2": data.logNow
						}
					}
				);

				DB.Insert(
					THIS.db + "_Log",
					{
						_act: data._id,
						type: data.logNow.type,
						ip:   data.logNow.ip,
						time: data.logNow.time
					}
				);

				// THIS._LOG.Add(THIS._LOG.TYPE.Usr, 111, "更新账户状态成功");
			}
			catch (e) {
				dtd.reject(e);

				// THIS._LOG.Add(THIS._LOG.TYPE.Usr, 112, "更新用户状态失败");
			}
		}

		dtd.resolve(act);

		return dtd.promise();
	},
	ReLogin:          async function (token) {
		const THIS = this,
			  dtd  = jBD.Deferred(true);

		let act = THIS._info.actByToken[token];

		if (!act) dtd.reject(ERR(4));
		else {
			act = loginInfo(THIS._info, act, "", THIS.timeout, THIS.token);
			dtd.resolve(act);
		}

		return dtd.promise();
	},
	SSH:              async function (data) {
		const THIS = this,
			  DB   = THIS.get("db"),
			  dtd  = jBD.Deferred(true);

		try {
			let act = await actCheck(
				DB, THIS.db,
				{device: data.device, "ssh.token": data.token}
			);

			if (!act) dtd.reject(ERR(3));
			else if (act.state === STATE.Locked) dtd.reject(ERR(112, "账号被禁用"));
			else {
				act = await THIS.__doneByLogin(act, data.ip);

				dtd.resolve(act);
			}
		}
		catch (e) {
			dtd.reject(e);
		}

		return dtd.promise();
	},
	Refresh:          async function (token) {
		const THIS = this,
			  dtd  = jBD.Deferred(true),
			  act  = THIS._info.actByToken[token];

		if (act) {
			act.timeout = jBD.Date.IncMinute(THIS.timeout);

			dtd.resolve(act);
		}
		else dtd.reject(ERR(4));

		return dtd.promise();
	},
	Insert:           async function (data) {
		const THIS = this,
			  DB   = THIS.get("db"),
			  dtd  = jBD.Deferred(true);

		try {
			let rs = await THIS.Check(data, null, true);

			if (rs) dtd.reject(ERR(121, "账户已存在"));
			else {
				let opt = {safe: true},
					act;

				act = await THIS.__fillByInsert(data, data.id || null, opt);
				rs = await DB.Insert(THIS.db, act, opt);

				act._id = rs[0]._id;

				if (THIS.call("insert", [dtd, rs]) !== false) dtd.resolve(act);

				// THIS._LOG.Add(THIS._LOG.TYPE.Usr, 121, `[${rs[0].account}] 账户注册`);
			}
		}
		catch (e) {
			dtd.reject(e);
		}

		return dtd.promise();
	},
	Remove:           async function (data) {
		const THIS = this,
			  DB   = THIS.get("db"),
			  INFO = THIS._info,
			  dtd  = jBD.Deferred(true);

		try {
			let act = [];

			jBD.each(data, d => {
				if (!jBD.isString(d) || d.length != 24) return;

				act.push(d);

				THIS.__deleteByRemove(INFO, INFO.actById[d]);
			});

			if (act.length < 1) dtd.reject(ERR(-3));
			else {
				for (let i = 0; i < act.length; i++) act[i] = DB.ObjectID(act[i]);

				let rs = await DB.Remove(
					THIS.db,
					{
						_id: {
							"$in": act
						}
					}
				);

				if (THIS.call("remove", [dtd, rs, act]) !== false) dtd.resolve(act);
				// THIS._LOG.Add(THIS._LOG.TYPE.Usr, 131, "删除账户成功");
			}
		}
		catch (e) {
			dtd.reject(e);
			// THIS._LOG.Add(THIS._LOG.TYPE.Usr, 131, "删除账户失败");
		}

		return dtd.promise();
	},
	Update:           async function (id, data) {
		const THIS = this,
			  DB   = THIS.get("db"),
			  dtd  = jBD.Deferred(true);

		delete data.password;
		delete data.checkcode;

		try {
			let rs = await actUpdate(DB, THIS.db, id, data);

			dtd.resolve(rs);
		}
		catch (e) {
			dtd.reject(e);
		}

		return dtd.promise();
	},
	//========================================
	__fillByLogin:    function (data) {
		let dt     = new Date(),
			result = {
				_ud:     data._ud || dt,
				_id:     data._id,
				state:   STATE.Online,
				logReg:  data.log[0],
				logLast: data.log[1],
				logNow:  null,
				token:   null,
				timeout: null,
				account: data.account,
				device:  data.device || ""
			};

		if (data.ssh && data.device) {
			result.token = data.ssh.token || "";
			result.timeout = data.ssh.timeout || null;
		}

		return result;
	},
	__doneByLogin:    async function (data, ip, device) {
		const THIS = this,
			  DB   = THIS.get("db"),
			  dtd  = jBD.Deferred(true);

		try {
			let act = THIS.__fillByLogin(data),
				rs;

			if (device) act.device = device;

			act = loginInfo(THIS._info, act, ip, THIS.timeout, THIS.token);
			rs = await DB.Update(
				THIS.db,
				{_id: act._id},
				{
					"$set": {
						state:   act.state,
						"log.1": act.logNow,
						device:  act.device,
						ssh:     {
							token:   act.token,
							timeout: act.timeout
						}
					}
				}
			);

			logInfo(DB, THIS.db, act);

			if (THIS.call("login", [dtd, act]) !== false) dtd.resolve(act);

			// THIS._LOG.Add(THIS._LOG.TYPE.Usr, 111, "更新账户状态成功");
		}
		catch (e) {
			dtd.reject(e);

			// THIS._LOG.Add(THIS._LOG.TYPE.Usr, 112, "更新账户状态失败");
		}

		// THIS._LOG.Add(THIS._LOG.TYPE.None, 101, `[${act._id + ""}] 账户登入`);

		return dtd.promise();
	},
	__fillByInsert:   function (data, id, opt) {
		const info = {
			_ud:     null,
			state:   STATE.Inited,
			log:     [{type: 0, ip: data.ip, time: new Date()}, null, null],
			account: data.account,
			device:  data.device || "",
			ssh:     {
				token:   null,
				timeout: null
			},
			name:    data.name || this.name || "临时用户"
		};

		if (id) info.id = id;

		return info;
	},
	__deleteByRemove: function (info, act) {
		if (!act) return;

		delete info.actById[act._id + ""];
		delete info.actByToken[act.token];

		info.length--;
	},
	__deleteByLogout: function (info, data) {
		if (data.token) this.__deleteByRemove(info, info.actByToken[data.token]);

		data.state = STATE.Offline;
		data.logNow.time = new Date();
		data.logNow.type = 2;

		return data;
	},
	__checkByAccount: function (data, out, n) {
		return false;
	},
	__chkActByCheck:  async function (data, id) {
		const THIS = this,
			  DB   = THIS.get("db"),
			  dtd  = jBD.Deferred(true);

		if (id) data = {"$or": [data, {_id: DB.ObjectID(id)}]};

		try {
			let rs = await actCheck(DB, THIS.db, data);

			dtd.resolve(rs);
		}
		catch (e) {
			dtd.reject(e);
		}

		return dtd.promise();
	},
	__chkActByLogin:  async function (mode, act, data) {
		const dtd = jBD.Deferred(true);

		try {
			dtd.resolve(act);
		}
		catch (e) {
			dtd.reject(e);
		}

		return dtd.promise();
	}
});

(function (TAct) {
	const getPassword  = (pwd, cc) => {
			  return jBD.Security.MD5(pwd + cc);
		  },
		  getCheckCode = sn => {
			  return jBD.Security.MD5(sn + jBD.Conver.toString(new Date() + jBD.String.Random(6)), true);
		  };

	const TLocalAct = TAct.extend({
		className:        "TLocalAct",
		create:           function (cfg, db, chk) {
			this.super("create", [cfg, db], "TAct");

			this.account = cfg.account;

			this.set("chk", chk);
		},
		__fillByLogin:    function (data) {
			const info = this.super("__fillByLogin", [data]);

			info.checkcode = data.checkcode || "";

			return info;
		},
		__fillByInsert:   function (data, id, opt) {
			const THIS = this,
				  info = THIS.super("__fillByInsert", [data, id, opt]);

			switch (THIS.mode.password) {
				case "password":
					info.checkcode = getCheckCode(THIS.key || "jBD");
					info.password = getPassword(data.password || "e10adc3949ba59abbe56e057f20f883e", info.checkcode);
					break;
				case "checkcode":
					info.account = info.phone = data.account;
					break;
			}

			return info;
		},
		__checkByAccount: function (data, out, n) {
			const local   = this.account,
				  account = data.account;

			if (!account) return false;
			if (account.length < local[0] || account.length > local[1]) return false;

			if (out) out.account = account;

			return true;
		},
		__chkActByLogin:  async function (mode, act, data) {
			const THIS = this,
				  dtd  = jBD.Deferred(true);

			try {
				switch (mode.password) {
					case "password":
						if (getPassword(data.password, act.checkcode) === act.password) dtd.resolve(act);
						else dtd.reject(ERR(111, "密码错误"));
						break;
					case "checkcode":
						if (THIS.get("chk").Done(data.tag, data.phone, data.checkcode)) dtd.resolve(act);
						else dtd.reject(ERR(111, "验证码错误"));
						break;
				}
			}
			catch (e) {
				dtd.reject(e);
			}

			return dtd.promise();
		},
		__chkPassword:    async function (id, pwd) {
			const THIS = this,
				  DB   = THIS.get("db"),
				  dtd  = jBD.Deferred(true);

			try {
				let rs = await actCheck(DB, THIS.db, {_id: DB.ObjectID(id)});

				if (!rs) throw ERR(3);
				if (getPassword(pwd, rs.checkcode) !== rs.password) throw ERR(120, "密码不一致");

				dtd.resolve();
			}
			catch (e) {
				dtd.reject(e);
			}

			return dtd.promise();
		},
		Password:         async function (data) {
			const THIS = this,
				  DB   = THIS.get("db"),
				  dtd  = jBD.Deferred(true);

			try {
				if (data.oldpasswd) await THIS.__chkPassword(data.id, data.oldpasswd);

				let checkcode = getCheckCode(THIS.key || "jBD"),
					password  = getPassword(data.password, checkcode),
					rs;

				rs = await actUpdate(
					DB, THIS.db, data.id,
					{
						checkcode: checkcode,
						password:  password
					}
				);

				dtd.resolve(rs);
			}
			catch (e) {
				dtd.reject(e);
			}

			return dtd.promise();
		}
	});

	TLocalAct.Config = function (cfg, svr, file) {
		(mode => {
			switch (mode.account) {
				case "account":
					svr.account = (jBD.isArray(cfg.account) ? cfg.account : [4, 20]).concat([0, 0]);
					svr.account[0] = !jBD.isNumber(svr.account[0], {int: true}) || svr.account[0] < 1 ? 4 : svr.account[0];
					svr.account[1] = !jBD.isNumber(svr.account[1], {int: true}) || svr.account[1] < svr.account[0] ? svr.account[0] : svr.account[1];
					svr.account = [svr.account[0], svr.account[1]];
					break;
				case "phone":
					svr.account = [11, 11];
					break;
			}

			file.account = svr.account;
		})(svr.mode);
	};

	MODULE.TLocalAct = TLocalAct;
})(MODULE.TAct);

(function (TAct) {
	const getPaper = (count, limit) => {
		let result = [];

		limit--;

		for (let i = 0; i < count; i++) {
			result.push(jBD.String.Random({
				template: jBD.String.ULETTER,
				len:      parseInt(Math.random() * limit) + 1,
				repeat:   false
			}));
		}

		return result;
	};

	const TPaperAct = TAct.extend({
		className:        "TPaperAct",
		create:           function (cfg, db, chk) {
			this.super("create", [cfg, db], "TAct");

			this.paper = cfg.paper;

			this.set("chk", chk);
		},
		Paper:            async function (method, id, data) {
			const THIS = this,
				  DB   = THIS.get("db"),
				  dtd  = jBD.Deferred();

			try {
				if (!id || !jBD.isArray(data)) dtd.reject(ERR(3));
				else {
					let paper  = THIS.paper,
						select = {},
						rs;

					switch (method) {
						default:
							dtd.reject(ERR(3));
							break;
						case "check":
							let r = jBD.each(data, (d, v) => {
								if (!jBD.isObject(d)) return false;
								if (!jBD.isNumber(d.index, {int: true}) || d.index < 1 || d.index > paper.count) return false;
								if (!jBD.isString(d.code)) return false;

								v = "paper." + (d.index - 1);
								if (select[v]) return false;

								select[v] = d.code;
							});

							if (!r) dtd.reject(ERR(3));
							else {
								select._id = DB.ObjectID(id);

								rs = await actCheck(DB, THIS.db, select);

								if (!rs) dtd.reject(ERR(3));
								else if (rs.state === STATE.Locked) dtd.reject(ERR(112, "账号被禁用"));
								else dtd.resolve(rs._id);
							}
							break;
						case "new":
							select.paper = getPaper(paper.count, paper.limit);

							rs = await actUpdate(DB, THIS.db, id, select);

							dtd.resolve(rs);
							break;
					}
				}
			}
			catch (e) {
				dtd.reject(e);
			}

			return dtd.promise();
		},
		__fillByLogin:    function (data) {
			const info = this.super("__fillByLogin", [data]);

			info.paper = data.paper || [];

			return info;
		},
		__fillByInsert:   function (data, id, opt) {
			const THIS  = this,
				  paper = THIS.paper,
				  info  = THIS.super("__fillByInsert", [data, id, opt]);

			info.paper = getPaper(paper.count, paper.limit);

			delete info.account;

			opt.autoinc = {
				field:   "account",
				initial: 1001
			};

			return info;
		},
		__checkByAccount: function (data, out, n) {
			const local = this.paper,
				  paper = data.paper;

			if (!n) {
				if (!jBD.isArray(paper) || paper.length < 1 || paper.length > local.count) return false;
				if (!jBD.each(paper, d => {
					if (!jBD.isString(d) || d.length < 1 || d.length > local.limit) return false;
				})) return false;
			}

			if (out) out.paper = n ? 1 : paper;

			return true;
		}
	});

	TPaperAct.Config = function (cfg, svr, file) {
		((cfg, svr) => {
			svr.count = jBD.isNumber(cfg.count, {int: true}) && cfg.count > 0 ? cfg.count : 10;
			svr.limit = jBD.isNumber(cfg.limit, {int: true}) && cfg.limit > 0 ? cfg.limit : 6;
		})(jBD.isObject(cfg.paper) ? cfg.paper : {}, svr.paper = file.paper = {});
	};

	MODULE.TPaperAct = TPaperAct;
})(MODULE.TAct);

MODULE.Config = function (cfg, svr, file) {
	if (!jBD.isObject(cfg)) cfg = {};

	svr.db = file.db = jBD.isString(cfg.db) ? cfg.db : "Account_Person";
	svr.timeout = file.timeout = jBD.Conver.toInteger(cfg.timeout, 15);
	svr.key = file.key = jBD.isString(cfg.key) ? cfg.key : "jBD";
	svr.name = file.name = jBD.isString(cfg.name) ? cfg.name : "临时用户";
	svr.token = file.token = jBD.has(cfg.token, ["none", "account", "id", "checkcode", "time", "device"]) ? cfg.token : "none";

	((cfg, svr) => {
		svr.account = jBD.has(cfg.account, ["account", "phone", "paper"]) ? cfg.account : "account";
		svr.password = jBD.has(cfg.password, ["password", "checkcode", "paper"]) ? cfg.password : "password";
	})(jBD.isObject(cfg.mode) ? cfg.mode : {}, svr.mode = file.mode = {});

	switch (svr.mode.account) {
		case "paper":
			MODULE.TPaperAct.Config(cfg, svr, file);
			break;
		default:
			MODULE.TLocalAct.Config(cfg, svr, file);
			break;
	}
};

exports = module.exports = MODULE;