"use strict";

const TMW = jSvr.TMW,
	  ERR = jSvr.ERR;

function MODULE (cfg, db) {
	return new MODULE.TAuth(cfg, db);
}

const codeTimeout  = obj => {
		  function event () {
			  tid = 0;

			  try {
				  if (info.length < 1) return;

				  let now = new Date(),
					  k, v;

				  for (k in info.authByCode) {
					  v = info.authByCode[k];

					  if (v.timeout && v.timeout <= now) delete info.authByCode[k];
				  }
			  }
			  finally {
				  work();
			  }
		  }

		  function work () {
			  return tid = setTimeout(event, time);
		  }

		  let tid  = 0,
			  info = obj._info,
			  time = obj.timeout;

		  return function (state) {
			  if (state !== false) work();
			  else if (tid > 0) clearTimeout(tid);
		  };
	  },
	  generateCode = (act, list) => {
		  let code = "";

		  while (!code || list[code]) {
			  code = jBD.String.Random({
				  template: jBD.String.ULLN,
				  len:      24,
				  repeat:   true
			  });
		  }

		  list[code] = 1;

		  return code;
	  };

MODULE.TAuth = TMW.extend({
	className: "TAuth",
	create:    function (cfg, db) {
		let THIS = this;

		THIS.super("create", null, "TMW");

		THIS.db = cfg.db;
		THIS.timeout = cfg.timeout;

		THIS._info = {
			authByCode: {},
			length:     0
		};

		THIS
			.set("db", db);

		setTimeout(function () {
			THIS.Init(true);
		}, 5 * 1000);
	},
	Init:      async function (force) {
		const THIS = this,
			  DB   = THIS.get("db"),
			  INFO = THIS._info;

		try {
			let rs   = await DB.Find(THIS.db),
				code = force === true ? {} : INFO.authByCode,
				now  = new Date();

			INFO.authByCode = code;

			jBD.each(rs, d => {
				if (d.timeout < now) return;

				code[d.code] = {
					_act:    d._act,
					title:   d.title,
					timeout: d.timeout
				};
			});

			if (THIS.timeout < 1) THIS.set("timeout", codeTimeout(this)).get("timeout")();
		}
		catch (e) {
			console.error(e);
		}
	},
	Check:     function (key, sign, req) {
		let auth = this._info.authByCode[key];

		if (!auth || !sign) return false;
		if (this.__chkByRequest && !this.__chkByRequest(sign, req)) return false;

		return true;
	},
	Generate:  async function (id, title, timeout) {
		const THIS = this,
			  DB   = THIS.get("db"),
			  INFO = THIS._info,
			  dtd  = jBD.Deferred(true);

		let now = jBD.Conver.toString(new Date(), "yyyymmddhhnnss"),
			code, rs;

		id = DB.ObjectID(id);
		title = title || ("Auth-" + now);
		switch (jBD.type(timeout, true)) {
			case "date":
				break;
			default:
				timeout = 0;
			case "number":
				timeout = parseInt(timeout);
				if (timeout < 1) timeout = 365;
				timeout = jBD.Date.IncDay(timeout);
				break;
		}

		code = generateCode(id, INFO.authByCode);
		rs = await DB.Insert(THIS.db, {
			_act:    id,
			title:   title,
			timeout: timeout,
			code:    code,
		});

		if (!rs) {
			delete INFO.authByCode[code];

			dtd.reject(ERR(10, "生成授权码异常"));
		}
		else {
			INFO.authByCode[code] = {
				_act:    id,
				title:   title,
				timeout: timeout
			};

			dtd.resolve(code);
		}

		return dtd.promise();
	},
	Query:     async function (data) {
		const THIS = this,
			  DB   = THIS.get("db"),
			  dtd  = jBD.Deferred(true);

		let now    = new Date(),
			select = {},
			rs;

		if (data.act) select._act = DB.ObjectID(data.act);
		if (data.code) select.code = jBD.RegExp.Filter([data.code]);

		rs = await DB.Find(THIS.db, select, {_act: 1});

		select = [];

		jBD.each(rs, d => {
			select.push({
				id:      d._id + "",
				time:    DB.Time(d._id),
				act:     d._act + "",
				state:   d.timeout > now,
				title:   d.title,
				timeout: jBD.Conver.toString(d.timeout, "yyyy-mm-dd")
			});
		});

		dtd.resolve(select);

		return dtd.promise();
	},
	Remove:    async function (data) {
		const THIS = this,
			  DB   = THIS.get("db"),
			  INFO = THIS._info,
			  dtd  = jBD.Deferred(true);

		let select = {},
			rs;

		if (data.id) select._id = DB.ObjectID(data.id);
		if (data.act) select._act = DB.ObjectID(data.act);

		rs = await DB.Find(THIS.db, select, {fields: {code: 1}});
		select = [];

		jBD.each(rs, d => {
			delete INFO.authByCode[d.code];

			select.push(d._id);
		});

		await DB.Remove(THIS.db, {_id: {"$in": select}});

		dtd.resolve();

		return dtd.promise();
	}
});

MODULE.Config = function (cfg, svr, file) {
	if (!jBD.isObject(cfg)) cfg = {};

	svr.db = file.db = jBD.isString(cfg.db) ? cfg.db : "Auth";
	svr.timeout = file.timeout = jBD.Conver.toInteger(cfg.timeout, 15);
};

exports = module.exports = MODULE;