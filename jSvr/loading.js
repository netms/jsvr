/**
 * ==========================================
 * Name:           jSvr‘s Loading
 * Author:         Buddy-Deus
 * CreTime:        2018-07-11
 * Description:    jSvr Loading Core
 * Log
 * 2018-07-11    初始化
 * ==========================================
 */
(() => {
	jSvr.DB = require("./lib/db.js");
	jSvr.File = require("./lib/file.js");
	jSvr.Act = require("./lib/act.js");
	jSvr.Chk = require("./lib/chk.js");
	jSvr.Pay = require("./lib/pay.js");
	jSvr.Auth = require("./lib/auth.js");
})();