const jBD    = require("jbd"),
	  path   = require("path"),
	  cfg    = require("./config.json"),
	  global = require("./bin/global.js")(),
	  bin    = path.join(__dirname, "bin"),
	  out    = path.join(__dirname, "public");

(async function () {
	for (let i = 0, p, c; i < cfg.project.length && (p = cfg.project[i]); i++) {
		console.log("===== " + p + " =====\n");
		c = require(path.join(bin, p + ".js"));
		await c(global, cfg.info, path.join(__dirname, p), path.join(out, p));
		console.log("============================\n");
	}

	console.log("All OK\n");

	process.exit();
})();
