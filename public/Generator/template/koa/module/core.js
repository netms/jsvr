const ERR    = jSvr.ERROR,
	  TSvr   = jSvr.TSvr,
	  MODULE = function (plugins, cfg, url) {
		  if (THIS) return THIS;

		  {plugins2}

		  let core = (new MODULE.T{name}(cfg));

		  {plugins3}

		  return THIS;
	  };

let THIS, CFG;

{plugins1}

MODULE.T{name} = TSvr.extend({
	className: "T{name}",
	create:    function (app) {
		THIS = this;
		CFG = app;

		this.super("create");
	}
});

module.exports = MODULE;