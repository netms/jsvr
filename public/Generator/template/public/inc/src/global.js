"use strict";

jBD.ready(function () {
	bodyResize();
	navEvent();
});

function bodyResize () {
	$("html").resize(function () {
		$(this).css("font-size", ($(window).width() / 750 * 75) + "px");
	}).resize();
}

function touchEvent (margin, callback) {
	let x = 0,
		y = 0;

	$("body")
		.off()
		.on("touchstart", function (e) {
			x = e.originalEvent.changedTouches[0].pageX;
			y = e.originalEvent.changedTouches[0].pageY;
		})
		.on("touchmove", function (e, o, t, xn, yn) {
			o = e.originalEvent.changedTouches[0];

			xn = x - o.pageX;
			yn = y - o.pageY;

			if (xn > margin.x[0]) t = "left";
			else if (xn < margin.x[1]) t = "right";
			else if (yn > margin.y[0]) t = "up";
			else if (yn < margin.y[1]) t = "down";
			else return;

			if (callback) callback(t);

			x = o.pageX;
			y = o.pageY;
		});
}

function navEvent () {
	let header = $("header");

	$("nav.head .btn").click(function () {
		if ($(this).hasClass("close")) header.removeClass("show");
		else header.addClass("show");
	});
}

function http (type, url, data, headers) {
	if (!headers || typeof(headers) !== "object") headers = {};
	if (!data || typeof(data) !== "object") data = {};

	let callback = {},
		result   = {
			on:  (key, cb) => {
				callback[key] = cb;

				return result;
			},
			off: key => {
				delete callback[key];

				return result;
			}
		};

	callback.temp = jBD.callback(arguments);
	if (jBD.isFunction(callback.temp)) callback.done = callback.temp;
	delete callback.temp;

	switch (type) {
		case "get":
		case "post":
			$.ajax({
				url:      jBD.Request.Rmd(url, false),
				type:     type.toUpperCase(),
				dataType: "json",
				headers:  headers,
				data:     data,
				success:  function (d) {
					if (callback.done) callback.done(d);
				},
				error:    function (xhr) {
					if (callback.error) callback.error({errorCode: xhr.status, errorMsg: xhr.statusText});
				}
			});
			break;
		case "json":
			$.get(jBD.Request.Rmd(url, false), d => {
				if (d && callback.done) callback.done(d);
			});
			break;
		case "js":
			type = "script";
		case "script":
		case "text":
		case "html":
			$.ajax({
				url:      jBD.Request.Rmd(url, false),
				type:     "GET",
				dataType: type,
				headers:  headers,
				data:     data,
				success:  function (d) {
					if (callback.done) callback.done(d);
				},
				error:    function (xhr) {
					if (callback.error) callback.error("HTTP Code:" + xhr.status + ", " + xhr.statusText);
				}
			});
			break;
		case "file":
			(function () {
				var xhr = new XMLHttpRequest(),
					fd  = new FormData();

				xhr.upload.onprogress = function (e) {
					if (e.lengthComputable) {
						if (callback.progress) callback.progress(Math.round(e.loaded * 100 / e.total));
					}
					else {
						if (callback.error) callback.error();
					}
				};
				xhr.onload = function (e) {
					if (callback.done) callback.done(jBD.Conver.toJSON(e.target.responseText));
				};
				xhr.onerror = function (e) {
					console.error(e);
					if (callback.error) callback.error(e);
				};
				xhr.onabort = function (e) {
					console.warn(e);
					if (callback.abort) callback.abort(e);
				};

				jBD.each(data, function (d, k, t) {
					switch (t) {
						case "array":
							if (k === "file") jBD.each(data[k], d => fd.append(k, d));
						case "object":
						case "date":
							fd.append(k, jBD.Conver.toString(d));
							break;
						default:
							fd.append(k, d);
							break;
					}
				}, true);

				xhr.open("POST", url, true);
				xhr.send(fd);
			})(jBD.Request.Rmd(url, false));
			break;
	}

	return result;
}