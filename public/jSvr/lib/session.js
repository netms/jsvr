"use strict";

const Redis   = require("ioredis"),
	  Session = require("koa-session2"),
	  Store   = Session.Store;

class RedisStore extends Store {
	constructor (redis, timeout) {
		super();
		this.redis = redis;
		this.timeout = timeout * 1000;
	}

	async get (sid, ctx) {
		let data = await this.redis.get(sid);

		return JSON.parse(data);
	}

	async set (session, {sid = this.getID(24), maxAge = this.timeout || 1000000} = {}, ctx) {
		try {
			await this.redis.set(sid, JSON.stringify(session), "EX", maxAge / 1000);
		}
		catch (e) {}

		return sid;
	}

	async destroy (sid, ctx) {
		return await this.redis.del(sid);
	}
}

module.exports = function (cfg) {
	return Session({
		store: new RedisStore(new Redis(cfg.url), cfg.timeout),
		key:   "SESSION",
		sign:  true
	});
};