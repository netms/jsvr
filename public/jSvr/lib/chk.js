"use strict";

const fs   = require("fs"),
	  path = require("path"),
	  TMW  = jSvr.TMW,
	  ERR  = jSvr.ERR;

function MODULE (cfg) {
	switch (cfg.mode) {
		default:
		case "svg":
			return new MODULE.TSvgChk(cfg);
		case "ytx":
			return new MODULE.TYTXChk(cfg);
	}
}

const initChkInfo  = (list, info, type) => {
		  jBD.each(list, (d, k) => {
			  if (info[k]) return;
			  info[k] = {};
			  type.push(k);
		  });
	  },
	  getCheckCode = (len, list, any) => {
		  let code = jBD.String.Random({len: len, template: any ? jBD.String.ULLN : jBD.String.NUMBER});

		  return code;
	  },
	  tokenTimeout = (obj, ti) => {
		  function event () {
			  tid = 0;

			  try {
				  let now = new Date(),
					  type, key, v1, v2;

				  for (type in info) {
					  v1 = info[type];

					  for (key in v1) {
						  v2 = v1[key];

						  if (v2.timeout && v2.timeout <= now) {
							  if (obj.__chkDel) obj.__chkDel(obj.cfg.root, type, key);
							  delete v1[key];
						  }
					  }
				  }
			  }
			  finally {
				  work();
			  }
		  }

		  function work () {
			  return tid = setTimeout(event, ti);
		  }

		  let tid  = 0,
			  info = obj._info;

		  return function (state) {
			  if (state !== false) work();
			  else if (tid > 0) clearTimeout(tid);
		  };
	  };

MODULE.TChk = TMW.extend({
	className: "TChk",
	create:    function (cfg) {
		this.super("create", null, "TMW");

		this.len = cfg.len;
		this.timeout = cfg.timeout;
		this.any = cfg.any;
		this.key = cfg.key;

		this.TYPE = [];

		initChkInfo(this.timeout, this._info = {}, this.TYPE);

		this
			.set("timeout", tokenTimeout(this, 5000))
			.get("timeout")();
	},
	free:      function () {
		if (this.get("timeout")) this.get("timeout")(false);

		this._info = null;

		this.super("free", null, "TMW");
	},
	Check:     function (type, key) {
		let chk = this._info[type];

		return chk ? chk[key] : null;
	},
	Code:      async function (type, key) {
		const THIS = this,
			  dtd  = jBD.Deferred(true);

		try {
			let chk = THIS.Check(type, key),
				rs;

			if (chk) {
				rs = await THIS.__chkGet(chk, type);
				dtd.resolve({
					code:    10,
					key:     chk.key,
					text:    rs.code,
					timeout: rs.timeout,
					data:    rs.data
				});
			}
			else {
				let info = THIS._info[type];

				chk = {
					key:     key,
					code:    getCheckCode(THIS.len, info, THIS.any),
					timeout: jBD.Date.IncSecond(THIS.timeout[type] * 60)
				};

				// info[chk.phone] = chk;
				// dtd.resolve({
				// 	code:    chk.code,
				// 	phone:   chk.phone,
				// 	timeout: jBD.Date.DecSecond(chk.timeout)
				// });

				rs = await THIS.__chkSet(type, chk, THIS.timeout[type]);

				info[chk.key] = chk;

				dtd.resolve({
					code:    0,
					key:     chk.key,
					text:    chk.code,
					timeout: jBD.Date.DecSecond(chk.timeout),
					data:    rs.data
				});
			}
		}
		catch (e) {
			dtd.reject(e);
		}

		return dtd.promise();
	},
	Done:      function (type, key, code) {
		let chk = this.Check(type, key);

		if (!chk || chk.code != code) return false;

		if (this.__chkDel) this.__chkDel(this.cfg.root, type, key);
		delete this._info[type][key];

		return true;
	},
	__chkGet:  async function (chk, type) {
		return {
			code:    chk.code,
			timeout: jBD.Date.DecSecond(chk.timeout),
			data:    chk.code
		};
	},
	__chkSet:  async function (type, chk, tt) {}
});

(function (TChk) {
	const TSvgChk = TChk.extend({
		className: "TSvgChk",
		create:    function (cfg) {
			this.super("create", [cfg], "TChk");

			this.cfg = {
				root:   path.join(__dirname, "../data"),
				size:   cfg.len,
				ignore: cfg.ignore,
				noise:  cfg.noise,
				math:   cfg.math
			};

			this.oper = require("svg-captcha");

			jBD.FS.Make(this.cfg.root);
			jBD.FS.Remove(this.cfg.root, false);
		},
		__chkGet:  async function (chk, type) {
			const dtd = jBD.Deferred(true);

			try {
				let fn = path.join(this.cfg.root, `${type}_${chk.key}.svg`);

				fs.readFile(fn, (err, data) => {
					if (err) dtd.reject(ERR(10, "验证码生成错误"));
					else {
						dtd.resolve({
							code:    chk.code,
							timeout: jBD.Date.DecSecond(chk.timeout),
							data:    data.toString()
						});
					}
				});
			}
			catch (e) {
				dtd.reject(e);
			}

			return dtd.promise();
		},
		__chkSet:  async function (type, chk, tt) {
			const THIS = this,
				  dtd  = jBD.Deferred(true);

			try {
				let CFG  = THIS.cfg,
					OPER = THIS.oper,
					code;

				if (!CFG.math) {
					code = OPER(chk.code, {size: CFG.size, ignoreChars: CFG.ignore, noise: CFG.noise});
					code = {
						text: chk.code,
						data: code
					};
				}
				else {
					code = OPER.createMathExpr({noise: CFG.noise});
					chk.code = code.text;
				}

				fs.writeFile(path.join(CFG.root, `${type}_${chk.key}.svg`), code.data, err => {
					if (err) dtd.reject(ERR(10, "验证码生成错误"));
					else dtd.resolve({data: code.data});
				});
			}
			catch (e) {
				dtd.reject(e);
			}

			return dtd.promise();
		},
		__chkDel:  function (root, type, key) {
			let fn = path.join(root, `${type}_${key}.svg`);

			if (fs.existsSync(fn)) fs.unlink(fn);
		}
	});

	TSvgChk.Config = (cfg, svr, file) => {
		svr.math = file.math = cfg.math === true;
		svr.ignore = file.ignore = jBD.isString(cfg.ignore) ? cfg.ignore : "0o1i";
		svr.noise = file.noise = jBD.Conver.toInteger(cfg.noise, 4);

		svr.noise = file.noise = Math.max(svr.noise, 1);
		svr.noise = file.noise = Math.min(svr.noise, 3);

		if (svr.math) svr.any = file.any = false;
	};

	MODULE.TSvgChk = TSvgChk;
})(MODULE.TChk);

(function (TChk) {
	const TYTXChk = TChk.extend({
		className: "TYTXChk",
		create:    function (cfg) {
			this.super("create", [cfg], "TChk");

			this.cfg = {
				host:  cfg.host,
				port:  cfg.port,
				url:   cfg.url,
				sid:   cfg.sid,
				token: cfg.token,
				app:   cfg.app,
				tid:   cfg.tid
			};

			this.oper = require("https");
		},
		__chkSet:  async function (type, chk, tt) {
			const THIS = this,
				  dtd  = jBD.Deferred(true);

			try {
				let info = THIS._info[type];

				if (!info) dtd.reject(ERR(3));
				else {
					let cfg  = THIS.cfg,
						txt  = JSON.stringify({
							to:         chk.key,
							appId:      cfg.app,
							templateId: cfg.tid,
							datas:      [chk.code, tt + ""]
						}),
						time = jBD.Conver.toString(new Date(), "yyyymmddhhnnss"),
						req;

					req = this.oper.request(
						{
							hostname: cfg.host,
							port:     cfg.port,
							path:     jBD.String.Format(cfg.url, cfg.sid, jBD.Security.MD5(cfg.sid + cfg.token + time).toUpperCase()),
							method:   "POST",
							headers:  {
								"Accept":         "application/json",
								"Content-Type":   "application/json;charset=utf-8",
								"Content-Length": txt.length,
								"Authorization":  jBD.Security.Base64(cfg.sid + ":" + time)
							}
						},
						res => {
							txt = "";

							res.on("data", d => (txt += d));

							res.on("end", () => {
								try {
									if (!txt || (txt = JSON.parse(txt)).statusCode !== "000000") throw 0;

									chk.mid = txt.templateSMS.smsMessageSid;
									chk.mdc = txt.templateSMS.dateCreated;

									dtd.resolve({data: chk.code});
								}
								catch (e) {
									dtd.reject(ERR(11, "短信发送失败"));
								}
							});

							res.on("error", e => console.log(e));
						}
					);
					req.write(txt);
					req.end();
				}
			}
			catch (e) {
				dtd.reject(e);
			}

			return dtd.promise();
		}
	});

	TYTXChk.Config = (cfg, svr, file) => {
		svr.host = file.host = jBD.isString(cfg.host) ? cfg.host : "app.cloopen.com";
		svr.port = file.port = jBD.Conver.toInteger(cfg.port, 8883);
		svr.url = file.url = jBD.isString(cfg.url) ? cfg.url : "/2013-12-26/Accounts/{0}/SMS/TemplateSMS?sig={1}";

		svr.sid = file.sid = jBD.isString(cfg.sid) ? cfg.sid : "";
		svr.token = file.token = jBD.isString(cfg.token) ? cfg.token : "";
		svr.app = file.app = jBD.isString(cfg.app) ? cfg.app : "";
		svr.tid = file.tid = jBD.isString(cfg.tid) ? cfg.tid : "";

		svr.any = file.any = false;
	};

	MODULE.TYTXChk = TYTXChk;
})(MODULE.TChk);

MODULE.Config = function (cfg, svr, file) {
	if (!jBD.isObject(cfg)) cfg = {};

	svr.mode = file.mode = jBD.isString(cfg.mode) ? cfg.mode : "ytx";
	svr.key = file.key = jBD.isString(cfg.key) ? cfg.key : "jsvr";
	svr.timeout = file.timeout = {};
	jBD.each(jBD.isObject(cfg.timeout) ? cfg.timeout : {}, (d, k) => {
		svr.timeout[k] = file.timeout[k] = jBD.Conver.toInteger(d, 300);
	});
	svr.len = file.len = jBD.Conver.toInteger(cfg.len, 6);
	svr.len = file.len = Math.max(svr.len, 4);
	svr.len = file.len = Math.min(svr.len, 8);
	svr.any = file.any = cfg.any !== false;

	switch (svr.mode) {
		default:
		case "svg":
			return MODULE.TSvgChk.Config(cfg, svr, file);
		case "ytx":
			return MODULE.TYTXChk.Config(cfg, svr, file);
	}
};

exports = module.exports = MODULE;