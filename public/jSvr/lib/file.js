"use strict";

const fs   = require("fs"),
	  path = require("path"),
	  FS   = jBD.FS,
	  TMW  = jSvr.TMW,
	  ERR  = jSvr.ERR;

function MODULE (cfg, db, url) {
	return new MODULE.TFile(cfg, db, url);
}

const urlencode  = value => {
		  value = encodeURIComponent(value + "")
			  .replace(/!/g, "%21")
			  .replace(/'/g, "%27")
			  .replace(/\(/g, "%28")
			  .replace(/\)/g, "%29")
			  .replace(/\*/g, "%2A")
			  .replace(/%20/g, "+");

		  return value;
	  },
	  fileExt    = fn => path.extname(fn).toUpperCase(),
	  fileDelete = fn => {
		  if (fn && fs.existsSync(fn)) fs.unlinkSync(fn);
	  },
	  fileTemp   = (dtd, fd, root) => {
		  fd = path.join(root, fd);

		  if (!fs.existsSync(fd)) dtd.reject(ERR(11, "文件丢失！"));
		  else {
			  let size = fs.statSync(fd).size,
				  name = path.basename(fd);

			  dtd.resolve({
				  head: {
					  "Content-Type":        "application/octet-stream;charset=utf8",
					  "Coneten-Length":      size,
					  "Content-Disposition": "attachment;filename=" + urlencode(name),
					  "Pragma":              "No-cache",
					  "Cache-Control":       "No-cache",
					  "Expires":             0
				  },
				  name: name,
				  size: size,
				  path: fd
			  });
		  }
	  },
	  fileInfo   = async (DB, db, id, type, key) => {
		  const dtd = jBD.Deferred(true);

		  try {
			  let _db = db,
				  rs;

			  switch (key) {
				  default:
				  case "path":
					  _db += "_File";
					  break;
				  case "temp":
					  _db += "_Temp";
					  break;
			  }

			  rs = await DB.FindOne(
				  _db,
				  {_id: DB.ObjectID(id)},
				  {fields: {type: 1, name: 1, path: 1, url: 1, size: 1}}
			  );

			  if (!rs) dtd.reject(ERR(3));
			  else if (type && rs.type !== type) dtd.reject(ERR(3));
			  else if (!fs.existsSync(rs.path)) dtd.reject(ERR(11, "文件丢失"));
			  else {
				  dtd.resolve({
					  head: {
						  "Content-Type":        "application/octet-stream;charset=utf8",
						  "Coneten-Length":      rs.size,
						  "Content-Disposition": "attachment;filename=" + urlencode(rs.name),
						  "Pragma":              "No-cache",
						  "Cache-Control":       "No-cache",
						  "Expires":             0
					  },
					  name: rs.name,
					  size: rs.size,
					  path: rs.path
				  });
			  }
		  }
		  catch (e) {
			  dtd.reject(e);
		  }

		  return dtd.promise();
	  },
	  pCheck     = async (DB, db, id) => {
		  const dtd = jBD.Deferred(true);

		  try {
			  let rs = await DB.FindOne(db + "_Temp", {_id: DB.ObjectID(id)});

			  if (rs) dtd.resolve(rs);
			  else dtd.reject(ERR(3));
		  }
		  catch (e) {
			  dtd.reject(e);
		  }

		  return dtd.promise();
	  },
	  pTemp      = async (DB, db, temp, url, id, list) => {
		  const dtd = jBD.Deferred(true);

		  try {
			  let data = [];

			  if (id) id = DB.ObjectID(id);
			  jBD.each(list, d => {
				  if (!d) return;
				  d._act = id;
				  data.push(d);
			  });

			  if (data.length < 1) dtd.resolve([]);
			  else {
				  let rs = await DB.Insert(db + "_Temp", data),
					  code;

				  code = [];

				  jBD.each(rs, (d, i, o) => {
					  o = data[i];

					  code.push({
						  id:  o.path ? (d._id + "") : "",
						  url: `${url}/temp?k=${d._id}`
					  });
				  });

				  dtd.resolve(code);
			  }
		  }
		  catch (e) {
			  dtd.reject(e);
		  }

		  return dtd.promise();
	  },
	  pClear     = async (DB, db, work) => {
		  const dtd = jBD.Deferred(true);

		  try {
			  let rs = await DB.Find(
				  db + "_Temp",
				  {path: {"$ne": null}},
				  {fields: {path: 1}}
			  );

			  jBD.each(rs, d => work(d.path));

			  dtd.resolve(rs.length);

			  DB.Remove(db + "_Temp");
		  }
		  catch (e) {
			  dtd.reject(e);
		  }

		  return dtd.promise();
	  },
	  pSave      = async (DB, db, data, list, url) => {
		  const dtd = jBD.Deferred(true);

		  try {
			  let rs = await DB.Insert(db + "_File", data);

			  jBD.each(rs, (d, i, r) => {
				  r = list[i];
				  r.id = d._id + "";
				  r.url = `${url}/path?k=${r.id}`;
			  });

			  dtd.resolve(list);
		  }
		  catch (e) {
			  dtd.reject(e);
		  }

		  return dtd.promise();
	  },
	  pRemove    = (DB, db, tmp, list) => {
		  DB.Remove(db + (tmp ? "_Temp" : ""), {_id: {"$in": list}});
	  };

MODULE.TFile = TMW.extend({
	className: "TFile",
	create:    function (cfg, db, url) {
		this.super("create", null, "TMW");

		this.url = url || cfg.url;
		this.file = cfg.file;
		this.db = cfg.db;
		this.timeout = cfg.timeout;

		this.TYPE = ["portrait", "passport"];

		this
			.set("db", db);
	},
	Check:     async function (id) {
		const THIS = this,
			  dtd  = jBD.Deferred(true);

		try {
			let rs = await pCheck(THIS.get("db"), THIS.db, id);

			dtd.resolve(rs);
		}
		catch (e) {
			dtd.reject(e);
		}

		return dtd.promise();
	},
	Info:      async function (id, type, key) {
		const THIS = this,
			  dtd  = jBD.Deferred(true);

		try {
			let rs = await fileInfo(THIS.get("db"), THIS.db, id, type, key);

			dtd.resolve(rs);
		}
		catch (e) {
			dtd.reject(e);
		}

		return dtd.promise();
	},
	Upload:    async function (id, type, list) {
		const THIS = this,
			  dtd  = jBD.Deferred(true);

		try {
			let dt     = jBD.Conver.toString(new Date(), "yyyymmdd"),
				result = [],
				rs;

			if (!jBD.isArray(list)) list = [list];

			if (list.length < 1) dtd.reject(ERR(-3));
			else {
				let temp = THIS.file.temp,
					url  = THIS.url;

				jBD.each(list, (f, k, o) => {
					result.push(o = {
						time: new Date(),
						type: type,
						md5:  f.filename,
						size: f.size,
						name: f.originalname,
						ext:  fileExt(f.originalname),
						path: null
					});

					try {
						o.path = path.join(
							temp, id + "", dt,
							`${jBD.Conver.toString(o.time, "hhnnsszzz")}${o.md5}${o.ext}`
						);

						FS.Oper(f.path, o.path, true);
					}
					catch (e) {
						fileDelete(f.path);
						o.path = null;
					}
				});

				rs = await pTemp(THIS.get("db"), THIS.db, temp, url, id, result);

				dtd.resolve(rs);
			}
		}
		catch (e) {
			dtd.reject(e);
		}

		return dtd.promise();
	},
	Archive:   async function (list, p) {
		const THIS    = this,
			  DB      = THIS.get("db"),
			  dtd     = jBD.Deferred(true),
			  surplus = {
				  count:  list.length,
				  url:    THIS.url,
				  list:   [],
				  data:   [],
				  remove: []
			  },
			  done    = async () => {
				  if (surplus.list.length < 1) dtd.reject(ERR(10, "内容为空"));
				  else {
					  let rs = await pSave(DB, THIS.db, surplus.data, surplus.list, surplus.url);

					  dtd.resolve(rs);

					  pRemove(DB, THIS.db, true, surplus.remove);
				  }
			  },
			  chk     = async (data) => {
				  try {
					  let d   = await THIS.Check(data),
						  tmp = d.path,
						  fn  = path.join(p, path.basename(tmp)),
						  ph  = path.join(THIS.file.path, fn);

					  FS.Oper(tmp, ph, true);

					  surplus.list.push({
						  id:   "",
						  path: ph,
						  url:  ""
					  });

					  surplus.data.push({
						  _act: d._act,
						  type: d.type,
						  time: d.time,
						  md5:  d.md5,
						  size: d.size,
						  name: d.name,
						  ext:  d.ext,
						  path: ph
					  });

					  surplus.remove.push(d._id);
				  }
				  catch (e) {
				  }

				  surplus.count--;
				  if (surplus.count < 1) done();
			  };

		try {
			if (list.length < 1) done();
			else {
				for (let i = 0; i < list.length; i++) chk(list[i]);
			}
		}
		catch (e) {
			dtd.reject(e);
		}

		return dtd.promise();
	},
	Remove:    async function (list) {
		const THIS = this,
			  DB   = THIS.get("db"),
			  dtd  = jBD.Deferred(true);

		let i = 0;
		while (i < list.length) {
			if (!list[i]) list.splice(i, 1);
			else i++;
		}

		if (list.length > 0) {
			pRemove(DB, THIS.db, true, list);
			pRemove(DB, THIS.db, false, list);
		}

		dtd.resolve();

		return dtd.promise();
	},
	Clear:     async function () {
		const THIS = this,
			  dtd  = jBD.Deferred();

		try {
			await pClear(THIS.get("db"), THIS.db, f => fileDelete(f));

			FS.Remove(THIS.file.temp, false);

			dtd.resolve();
		}
		catch (e) {
			dtd.reject(e);
		}

		return dtd.promise();
	}
});

MODULE.Config = function (cfg, svr, file, opt, url) {
	if (!jBD.isObject(cfg)) cfg = {};

	svr.db = file.db = jBD.isString(cfg.db) ? cfg.db : "File";
	svr.timeout = file.timeout = jBD.Conver.toInteger(cfg.timeout, 15);
	svr.file = opt;
	svr.url = url;

	if (svr.file.root) {
		if (!fs.existsSync(svr.file.root)) FS.Make(svr.file.root);
	}

	if (!svr.file.temp) {
		svr.file.temp = path.join(svr.file.root, "temp");
	}
	if (!fs.existsSync(svr.file.temp)) FS.Make(svr.file.temp);

	if (!svr.file.path) {
		svr.file.path = path.join(svr.file.root, "path");
	}
	if (!fs.existsSync(svr.file.path)) FS.Make(svr.file.path);

	return svr;
};

exports = module.exports = MODULE;