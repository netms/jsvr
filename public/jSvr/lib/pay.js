"use strict";

const https = require("https"),
	  url   = require("url"),
	  TMW   = jSvr.TMW,
	  ERR   = jSvr.ERR;

function MODULE (cfg, db) {
	return new MODULE.TPay(cfg, db);
}

const STATE       = {
		  Closed:           -3,
		  Inited:           -2,
		  Error:            -1,
		  Prepay:           0,
		  Pay:              1,
		  Payed:            2,
		  Refund:           3,
		  Refunded_Success: 4,
		  Refunded_Change:  5,
		  Refunded_Close:   6
	  },
	  jsonToxml   = json => {
		  let result = "";
		  for (let k in json) result += jBD.String.Format("<{0}><![CDATA[{1}]]></{0}>", [k, json[k]]);

		  return "<xml>" + result + "</xml>";
	  },
	  xmlTojson   = xml => {
		  let rxp    = /<(\D+)>((<\!\[CDATA\[(\S+)\]\]>)|(\S+))<\/\1>/g,
			  result = {},
			  v;

		  xml = jBD.String.Trim(xml, ["<xml>", "</xml>"]);
		  while (v = rxp.exec(xml)) result[v[1]] = v[4] || v[2];

		  return result;
	  },
	  pSTATE      = state => {
		  switch (state) {
			  case STATE.Closed:
				  return "订单关闭";
			  case STATE.Inited:
				  return "订单初始化";
			  default:
			  case STATE.Error:
				  return "订单错误";
			  case STATE.Prepay:
				  return "预付款发起";
			  case STATE.Pay:
				  return "付款中";
			  case STATE.Payed:
				  return "已付款";
			  case STATE.Refund:
				  return "退款中";
			  case STATE.Refunded:
				  return "已退款";
		  }
	  },
	  pTYPE       = state => {
		  switch (state) {
			  case "wechat":
				  return "微信支付";
			  case "alipay":
				  return "支付宝";
			  default:
				  return "未知模式";
		  }
	  },
	  pPay_Insert = async (DB, db, type, data, act) => {
		  const dtd = jBD.Deferred(true);

		  try {
			  let rs = await DB.Insert(
				  db,
				  {
					  state:   STATE.Inited,
					  type:    type,
					  _act:    act._id,
					  _ip:     act.logNow.ip,
					  _tag:    data.tag,
					  _pid:    DB.ObjectID(data._id),
					  _tid:    null,
					  title:   data.title,
					  intro:   data.intro,
					  money:   data.money,
					  subjoin: data.subjoin || {},
					  data:    null
				  }
			  );

			  dtd.resolve(rs);
		  }
		  catch (e) {
			  dtd.reject(e);
		  }

		  return dtd.promise();
	  },
	  pPay_Find   = async (DB, db, type, data, act) => {
		  const dtd = jBD.Deferred(true);

		  try {
			  let rs = await DB.FindOne(db, {
				  _id:   DB.ObjectID(data.out_trade_no),
				  _act:  act._id,
				  state: STATE.Payed
			  });

			  dtd.resolve(rs);
		  }
		  catch (e) {
			  dtd.reject(e);
		  }

		  return dtd.promise();
	  };

const pWeChat_Done         = () => {
		  return "<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>";
	  },
	  pWeChat_Error        = msg => {
		  return "<xml><return_code><![CDATA[FAIL]]></return_code><return_msg><![CDATA[" + (msg || "") + "]]></return_msg></xml>";
	  },
	  pWeChat_Sign         = (type, key, data) => {
		  let result = [],
			  k;

		  for (k in data) {
			  if (data[k] && k != "sign") result.push(k);
		  }
		  result.sort();
		  for (k = 0; k < result.length; k++) {
			  result[k] += "=" + data[result[k]];
		  }
		  result.push("key=" + key);
		  result = result.join("&");

		  switch (type) {
			  default:
			  case "MD5":
				  return jBD.Security.MD5(result).toUpperCase();
		  }
	  },
	  pWeChat_Rmd          = () => {
		  return jBD.Conver.toString(new Date(), "yyyymmddhhnnsszzz") + jBD.String.Random({
			  len:      10,
			  template: jBD.String.ULLN
		  });
	  },
	  pWeChat_Send         = async (DB, db, id, uri, data) => {
		  const dtd   = jBD.Deferred(true),
				error = err => {
					dtd.reject(err);

					DB.Update(db, id, {"$set": {state: STATE.Error, data: err}});
				};

		  let req = url.parse(uri);

		  req.method = "POST";
		  req.port = 443;

		  req = https.request(req, res => {
			  res.setEncoding("utf8");
			  res.on("data", json => {
				  if (!(json = xmlTojson(json.toString()))) error({errorCode: -1, errorMsg: "网络问题，返回异常"});
				  else if (json.return_code !== "SUCCESS") error({errorCode: 1, errorMsg: json.return_msg});
				  else if (json.result_code !== "SUCCESS") {
					  switch (json.err_code) {
						  default:
							  error({errorCode: 10, errorMsg: json.err_code_des});
							  break;
						  case "NOAUTH":
						  case "NOTENOUGH":
							  error({errorCode: 11, errorMsg: "用户余额不足！"});
							  break;
						  case "ORDERPAID":
							  error({errorCode: 12, errorMsg: "商户订单已支付，无需重复操作！"});
							  break;
						  case "ORDERCLOSED":
							  error({errorCode: 13, errorMsg: "当前订单已关闭，无法支付！"});
							  break;
						  case "SYSTEMERROR":
						  case "APPID_NOT_EXIST":
						  case "MCHID_NOT_EXIST":
						  case "APPID_MCHID_NOT_MATCH":
						  case "SIGNERROR":
						  case "LACK_PARAMS":
							  error({errorCode: 14, errorMsg: "订单状态异常！"});
							  break;
					  }
				  }
				  else {
					  dtd.resolve({
						  appid:      json.appid,
						  mch_id:     json.mch_id,
						  nonce_str:  json.nonce_str,
						  sign:       json.sign,
						  trade_type: json.trade_type,
						  prepay_id:  json.prepay_id
					  });
				  }
			  });
		  });
		  req.write(data);
		  req.end();

		  return dtd.promise();
	  },
	  pWeChat_Prepay       = async (DB, db, cfg, info) => {
		  const dtd = jBD.Deferred(true);

		  try {
			  let id   = {_id: info._id},
				  act  = {
					  _oid:  info._id,
					  _act:  info._act,
					  _pid:  info._pid,
					  state: STATE.Prepay,
					  type:  "wechat"
				  },
				  data = {
					  appid:            cfg.appid,
					  mch_id:           cfg.mch_id,
					  notify_url:       cfg.notify.pay,
					  device_info:      cfg.device_info,
					  trade_type:       cfg.trade_type,
					  sign_type:        cfg.sign_type,
					  fee_type:         cfg.fee_type,
					  nonce_str:        pWeChat_Rmd(),
					  out_trade_no:     info._id + "",
					  spbill_create_ip: info._ip,
					  body:             info.intro,
					  total_fee:        info.money * 100
				  };

			  data.sign = pWeChat_Sign(cfg.sign_type, cfg.key, data);

			  DB.Insert(db + "_History", jBD.clone(data, act, {write: false}));

			  let rs   = await DB.Update(db, id, {"$set": {state: STATE.Prepay, data: data}}),
				  json = await pWeChat_Send(DB, db, id, cfg.url.prepay, jsonToxml(data)),
				  result;

			  result = {
				  appid:     json.appid,
				  partnerid: json.mch_id,
				  package:   "Sign=WXPay",
				  noncestr:  pWeChat_Rmd(),
				  timestamp: (new Date()).getTime(),
				  prepayid:  json.prepay_id
			  };
			  result.sign = pWeChat_Sign(cfg.sign_type, cfg.key, result);

			  rs = await DB.Update(db, id, {"$set": {state: STATE.Pay, data: result}});

			  dtd.resolve(result);

			  act.state = STATE.Pay;
			  DB.Insert(db + "_History", jBD.clone(result, act, {write: false}));
		  }
		  catch (e) {
			  dtd.reject(e);
		  }

		  return dtd.promise();
	  },
	  pWeChat_Refund       = async (DB, db, cfg, info) => {
		  const dtd = jBD.Deferred(true);

		  try {
			  let id   = {_id: info._id},
				  act  = {
					  _oid:  info._id,
					  _act:  info._act,
					  _pid:  info._pid,
					  state: STATE.Prepay,
					  type:  "wechat"
				  },
				  data = {};

			  data.sign = pWeChat_Sign(cfg.sign_type, cfg.key, data);

			  DB.Insert(db + "_History", jBD.clone(data, act, {write: false}));

			  let rs = await DB.Update(db, id, {"$set": {state: STATE.Prepay, data: data}});
		  }
		  catch (e) {
			  dtd.reject(e);
		  }

		  return dtd.promise();
	  },
	  pWeChat_Close        = async (DB, db, cfg, info) => {},
	  pWeChat_Query_Refund = async (DB, db, data, act) => {},
	  pWeChat_Query_Order  = async (DB, db, data, act) => {},
	  pWeChat_Payed        = async function (dtd, cfg, info) {
		  const THIS = this,
				DB   = THIS.get("db"),
				db   = THIS.db;

		  info = xmlTojson(info);
		  if (info.return_code !== "SUCCESS") dtd.reject(pWeChat_Error(info.return_msg));
		  else if (info.result_code !== "SUCCESS") dtd.reject(pWeChat_Error(info.err_code_des));
		  else if (info.sign !== pWeChat_Sign(cfg.sign_type, cfg.key, info)) dtd.reject(pWeChat_Error("签名失败"));
		  else {
			  try {
				  let rs = await DB.FindOne(db, {_id: DB.ObjectID(info.out_trade_no), state: STATE.Pay});

				  if (!rs) dtd.reject(pWeChat_Error("记录不存在"));
				  else if (String(rs.money * 100) != info.total_fee) dtd.reject(pWeChat_Error("付款金额不符"));
				  else {
					  dtd.resolve(pWeChat_Done());

					  DB.Update(db,
						  {_id: rs._id},
						  {
							  "$set": {
								  state: STATE.Payed,
								  data:  info
							  }
						  }
					  )
						  .done(() => {
							  THIS.call("payed", rs._id + "");
						  });

					  info = jBD.clone(
						  {
							  _oid:  rs._id,
							  _act:  rs._act,
							  _pid:  rs._pid,
							  state: STATE.Payed,
							  type:  "wechat"
						  },
						  info,
						  {write: false}
					  );
					  DB.Insert(db + "_History", info);
				  }
			  }
			  catch (e) {
				  dtd.reject(pWeChat_Error("参数格式校验错误"));
			  }
		  }
	  },
	  pWeChat_Refunded     = async function (dtd, cfg, info) {
		  const THIS = this,
				DB   = THIS.get("db"),
				db   = THIS.db;

		  info = xmlTojson(info);
		  if (info.return_code !== "SUCCESS") dtd.reject(pWeChat_Error(info.return_msg));
		  else {
			  let key = jBD.Security.MD5(cfg.key).toLowerCase(),
				  msg = jBD.Security.Base64(info.req_info, false),
				  rs;

			  try {
				  info = jBD.Security.AEC(key, msg, {enc: false, level: 256});
				  rs = await DB.FindOne(db, {_id: DB.ObjectID(info.out_trade_no)});

				  if (!rs) dtd.reject(pWeChat_Error("记录不存在"));
				  else if (rs.state > STATE.Refund) dtd.resolve(pWeChat_Done());
				  else if (rs.state < STATE.Refund) dtd.reject(pWeChat_Error("状态错误"));
				  else {
					  let refund = {},
						  state;

					  switch (info.refund_status) {
						  case "SUCCESS":
							  state = STATE.Refunded_Success;
							  break;
						  case "CHANGE":
							  state = STATE.Refunded_Change;
							  break;
						  case "REFUNDCLOSE":
							  state = STATE.Refunded_Close;
							  break;
					  }

					  if (state === STATE.Refunded_Close) {
						  switch (info.refund_account) {
							  case "REFUND_SOURCE_RECHARGE_FUNDS":
								  refund.source = "可用余额退款/基本账户";
								  break;
							  case "REFUND_SOURCE_UNSETTLED_FUNDS":
								  refund.source = "未结算资金退款";
								  break;
						  }
						  refund.time = info.success_time;
						  refund.money = info.settlement_refund_fee;
						  refund.account = info.refund_recv_accout;
					  }

					  await DB.Update(db, {_id: rs._id}, {"$set": {state: state, refund: refund, data: info}});

					  dtd.resolve(pWeChat_Done());

					  THIS.call("refunded", rs._id + "");

					  info = jBD.clone(
						  {
							  _oid:  rs._id,
							  _act:  rs._act,
							  _pid:  rs._pid,
							  state: state,
							  type:  "wechat"
						  },
						  info,
						  {write: false}
					  );
					  DB.Insert(db + "_History", info);
				  }
			  }
			  catch (e) {
				  dtd.reject(pWeChat_Error("参数格式校验错误"));
			  }

		  }
	  };

const pAliply_Send         = async (DB, db, id, uri, data) => {},
	  pAlipay_Prepay       = async (DB, db, cfg, info) => {
		  const dtd = jBD.Deferred(true);

		  dtd.reject(ERR(3));

		  return dtd.promise();
	  },
	  pAlipay_Refund       = async (DB, db, cfg, info) => {

	  },
	  pAlipay_Close        = async (DB, db, cfg, info) => {},
	  pAlipay_Query_Refund = async (DB, db, cfg, data, act) => {},
	  pAlipay_Query_Order  = async (DB, db, cfg, data, act) => {},
	  pAlipay_Payed        = async function (dtd, cfg, info) {
		  dtd.reject(ERR(3));
	  },
	  pAlipay_Refunded     = async function (dtd, cfg, info) {
		  dtd.reject(ERR(3));
	  };

MODULE.TPay = TMW.extend({
	className:    "TPay",
	create:       function (cfg, db) {
		this.super("create", null, "TMW");

		this.db = cfg.db;
		this.port = cfg.port;
		this.wechat = cfg.wechat;
		this.alipay = cfg.alipay;

		this
			.set("db", db);
	},
	Prepay:       async function (type, data, act) {
		const THIS = this,
			  DB   = THIS.get("db"),
			  dtd  = jBD.Deferred(true);

		try {
			let CFG = THIS[type],
				FUNC;

			switch (type) {
				default:
					dtd.reject(ERR(3));
					return;
				case "wechat":
					FUNC = pWeChat_Prepay;
					break;
				case "alipay":
					FUNC = pAlipay_Prepay;
					break;
			}

			if (!CFG.state) dtd.reject(ERR(3));
			else {
				let rs = await pPay_Insert(DB, THIS.db, type, data, act);

				if (!rs || rs.length < 1) dtd.reject(ERR(3));
				else {
					rs = await FUNC(DB, THIS.db, CFG, rs[0]);

					dtd.resolve(rs);
				}
			}
		}
		catch (e) {
			dtd.reject(e);
		}

		return dtd.promise();
	},
	Refund:       async function (type, data, act) {
		const THIS = this,
			  DB   = THIS.get("db"),
			  dtd  = jBD.Deferred(true);

		try {
			let CFG = THIS[type],
				FUNC;

			switch (type) {
				default:
					dtd.reject(ERR(3));
					break;
				case "wechat":
					FUNC = pWeChat_Refund;
					break;
				case "alipay":
					FUNC = pAlipay_Refund;
					break;
			}

			if (!CFG.state) dtd.reject(ERR(3));
			else {
				let rs = await pPay_Find(DB, THIS.db, type, data, act);

				if (!rs) dtd.reject(ERR(3));
				else {
					rs = await FUNC(DB, THIS.db, CFG, rs);

					dtd.resolve(rs);
				}
			}
		}
		catch (e) {
			dtd.reject(e);
		}

		return dtd.promise();
	},
	Close:        async function (type, data, act) {
		const THIS = this,
			  DB   = THIS.get("db"),
			  dtd  = jBD.Deferred(true);

		try {
			let CFG = THIS[type],
				FUNC;

			switch (type) {
				default:
					dtd.reject(ERR(3));
					break;
				case "wechat":
					FUNC = pWeChat_Close;
					break;
				case "alipay":
					FUNC = pAlipay_Close;
					break;
			}

			if (!CFG.state) dtd.reject(ERR(3));
			else {
				let rs = await pPay_Find(DB, THIS.db, type, data, act);

				if (!rs) dtd.reject(ERR(3));
				else {
					rs = await FUNC(DB, THIS.db, CFG, rs);

					dtd.resolve(rs);
				}
			}
		}
		catch (e) {
			dtd.reject(e);
		}

		return dtd.promise();
	},
	Query_Order:  async function (type, data, act) {
		const THIS = this,
			  DB   = THIS.get("db"),
			  dtd  = jBD.Deferred(true);

		try {
			let CFG = THIS[type],
				FUNC;

			switch (type) {
				default:
					dtd.reject(ERR(3));
					break;
				case "wechat":
					FUNC = pWeChat_Query_Order;
					break;
				case "alipay":
					FUNC = pAlipay_Query_Order;
					break;
			}

			if (!CFG.state) dtd.reject(ERR(3));
			else {
				let rs = await FUNC(DB, THIS.db, CFG, data, act);

				dtd.resolve(rs);
			}
		}
		catch (e) {
			dtd.reject(e);
		}

		return dtd.promise();
	},
	Query_Refund: async function (type, data, act) {
		const THIS = this,
			  DB   = THIS.get("db"),
			  dtd  = jBD.Deferred(true);

		try {
			let CFG = THIS[type],
				FUNC;

			switch (type) {
				default:
					dtd.reject(ERR(3));
					break;
				case "wechat":
					FUNC = pWeChat_Query_Refund;
					break;
				case "alipay":
					FUNC = pAlipay_Query_Refund;
					break;
			}

			if (!CFG.state) dtd.reject(ERR(3));
			else {
				let rs = await FUNC(DB, THIS.db, CFG, data, act);

				dtd.resolve(rs);
			}
		}
		catch (e) {
			dtd.reject(e);
		}

		return dtd.promise();
	},
	Payed:        async function (type, info) {
		let THIS = this,
			dtd  = jBD.Deferred(true);

		try {
			let PAYED;

			switch (type) {
				default:
					dtd.reject(ERR(3));
					return;
				case "wechat":
					PAYED = pWeChat_Payed;
					break;
				case "alipay":
					PAYED = pAlipay_Payed;
					break;
			}

			PAYED.call(THIS, dtd, THIS[type], info);
		}
		catch (e) {
			dtd.reject(e);
		}

		return dtd.promise();
	},
	Refunded:     function (type, info) {
		let dtd  = jBD.Deferred(),
			THIS = this,
			REFUND;

		switch (type) {
			default:
				dtd.reject(ERR(3));
				return;
			case "wechat":
				REFUND = pWeChat_Refunded;
				break;
			case "alipay":
				REFUND = pAlipay_Refunded;
				break;
		}

		REFUND.apply(THIS, [dtd, THIS[type], info]);

		return dtd.promise();
	},
	History:      function (act) {
		let dtd  = jBD.Deferred(),
			THIS = this,
			DB   = THIS.get("db");

		DB.Find(THIS.db + "_History", {_act: act}, {sort: {_id: 1}})
			.done(rs => {
				let result = [];

				jBD.each(rs, d => {
					result.push({
						id:    d._id + "",
						time:  DB.Time(d._id),
						state: pSTATE(d.state),
						type:  pTYPE(d.type)
					});
				});

				dtd.resolve(result);
			})
			.fail(err => dtd.reject(err));

		return dtd.promise();
	},
	Query:        function (data) {
		let dtd    = jBD.Deferred(),
			THIS   = this,
			DB     = THIS.get("db"),
			select = {},
			opt    = {sort: {_id: 1}, limit: data.limit || 20};

		if (!isNaN(data.state)) select.state = data.state;
		if (data.limit === -1) delete opt.limit;
		if (data.search) {
			select["$or"] = [
				{title: jBD.RegExp.Filter(data.search, "i")},
				{_tid: jBD.RegExp.Filter(data.search, "i")}
			];
		}
		if (data.act) select["_act"] = DB.ObjectID(data.act);
		switch (data.mode) {
			case "prev":
				if (data.id) {
					(select._id = {})["$lt"] = DB.ObjectID(data.id);
					opt.sort["_id"] = -1;
				}
				break;
			case "next":
				if (data.id) (select._id = {})["$gt"] = DB.ObjectID(data.id);
				break;
			case "dprev":
				break;
			case "dnext":
				opt.sort["_id"] = -1;
				break;
		}

		DB.Find(THIS.db, select, opt)
			.done(rs => {
				if (!rs) dtd.reject(ERR(3));
				else {
					let _data = [];

					jBD.each(rs, d => {
						_data.push({
							id:    d._id + "",
							time:  DB.Time(d._id),
							state: pSTATE(d.state),
							type:  pTYPE(d.type),
							tag:   d._tag,
							act:   d._act + "",
							ip:    d._ip + "",
							title: d.title,
							money: d.money
						});
					});

					if ((data.id && data.mode == "prev") || data.mode == "dnext") _data.reverse();

					dtd.resolve(_data);
				}
			})
			.fail(err => {
				dtd.reject(err);
			});

		return dtd.promise();
	},
	Info_Get:     function (id) {
		let dtd  = jBD.Deferred(),
			THIS = this,
			DB   = THIS.get("db");

		DB.FindOne(THIS.db, {_id: DB.ObjectID(id)})
			.done(rs => {
				if (!rs) dtd.reject(ERR(3));
				else {
					dtd.resolve({
						id:      rs._id + "",
						time:    DB.Time(rs._id),
						state:   pSTATE(rs.state),
						type:    pTYPE(rs.type),
						act:     rs._act + "",
						ip:      rs._ip + "",
						tag:     rs._tag || "",
						pid:     rs._pid + "",
						tid:     rs._tid || "",
						title:   rs.title,
						info:    rs.intro,
						money:   rs.money,
						subjoin: rs.subjoin || {}
					});
				}
			})
			.fail(err => dtd.reject(err));

		return dtd.promise();
	}
});

MODULE.Config = function (cfg, svr, file) {
	if (!jBD.isObject(cfg)) cfg = {};

	svr.db = file.db = jBD.isString(cfg.db) ? cfg.db : "Pay";
	if (cfg.ssl) {
		if (fs.existsSync(cfg.ssl.key) && fs.existsSync(cfg.ssl.cert)) {
			svr.ssl = file.ssl = {
				key:  cfg.ssl.key,
				cert: cfg.ssl.cert
			};
		}
		else cfg.ssl = null;
	}

	let _cfg;

	cfg.ssl = cfg.ssl ? "https" : "http";
	_cfg = cfg.wechat || {};
	if (_cfg.state === true) {
		svr.wechat = file.wechat = {
			state:       true,
			key:         _cfg.key || "",
			appid:       _cfg.appid || "",
			mch_id:      _cfg.mch_id || "",
			device_info: _cfg.device_info || "WEB",
			sign_type:   _cfg.sign_type || "MD5",
			fee_type:    _cfg.fee_type || "CNY",
			trade_type:  _cfg.trade_type || "APP",
		};

		_cfg = cfg.wechat.url || {};
		svr.wechat.url = file.wechat.url = {
			prepay: _cfg.prepay || "https://api.mch.weixin.qq.com/pay/unifiedorder",
			oquery: _cfg.query || "https://api.mch.weixin.qq.com/pay/orderquery",
			close:  _cfg.close || "https://api.mch.weixin.qq.com/pay/closeorder",
			refund: _cfg.refund || "https://api.mch.weixin.qq.com/secapi/pay/refund",
			rquery: _cfg.rquery || "https://api.mch.weixin.qq.com/pay/refundquery"
		};

		_cfg = cfg.wechat.notify || {};
		svr.wechat.notify = file.wechat.notify = {
			pay:    _cfg.pay || (cfg.ssl + "://127.0.0.1/api/pay/wechat/payed"),
			refund: _cfg.refund || (cfg.ssl + "://127.0.0.1/api/pay/wechat/refunded")
		};
	}
	else {
		svr.wechat = file.wechat = {state: false};
	}

	_cfg = cfg.alipay || {};
	if (_cfg.state === true) {
		svr.alipay = file.alipay = {
			state:     cfg.alipay.state === true,
			app_id:    cfg.alipay.app_id || "",
			format:    "JSON",
			charset:   "utf-8",
			sign_type: cfg.alipay.sign_type || "RSA2"
		};

		_cfg = cfg.alipay.url || {};
		svr.alipay.url = file.alipay.url = {
			prepay: _cfg.prepay || "https://openapi.alipay.com/gateway.do"
		};

		_cfg = cfg.alipay.notify || {};
		svr.alipay.notify = file.alipay.notify = {
			pay:    _cfg.pay || (cfg.ssl + "://127.0.0.1/api/pay/alipay/payed"),
			refund: _cfg.refund || (cfg.ssl + "://127.0.0.1/api/pay/alipay/refunded")
		};
	}

	return cfg;
};

exports = module.exports = MODULE;