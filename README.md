![jSvr](jSvr-logo.png)
##jSvr是什么?
针对jBD的服务插件，支持RESTful、 Web


##jSvr有哪些功能？

* `核心` —— 服务基础框架、配置管理器
* `Act` —— 用户模块，完成用户注册、登入等权限控制
* `File` —— 文件模块，完成文件上传、下载，加载操作
* `Chk` —— 短信模块，完成短信发送、验证
* `Pay` —— 支付模块，完成微信、支付宝第三方付费


##问题反馈
在使用中有任何问题，欢迎反馈给我，可以用以下联系方式跟我交流

* QQ: 31325423
* Mail：[@Buddy-Deus](mailto:31325423@qq.com)


##捐助开发者
本项目源于自身的强迫症，希望你喜欢我的作品，同时也能支持一下。
当然，有钱捧个钱场（直接加我的QQ给我发红包就好了），没钱捧个人场，谢谢各位。


##感激
感谢以下的项目,排名不分先后

* [Express](http://expressjs.com)

