let path = require("path"),
	fs   = require("fs"),
	func;

const complete = function (root, name, prompt) {
		  prompt = func.launch() ? ">" : "$";

		  console.log("");
		  console.log("	install dependencies");
		  console.log("		%s cd %s && npm install", prompt, root);
		  console.log("");
		  console.log("	run the app;");

		  if (func.launch()) {
			  console.log("		%s SET DEBUG=%s* & npm start", prompt, name);
		  }
		  else {
			  console.log("		%s DEBUG=%s:* npm start", prompt, name);
		  }

		  console.log("");
	  },
	  update   = function (root) {
		  let cfg = require(path.join(root, "./config.json")),
			  pkg = require(path.join(root, "./package.json"));

		  if (!cfg.page && !fs.existsSync(path.join(root, "./view")) && !fs.existsSync(path.join(root, "./public"))) return;

		  func.cpdir("../public", `${root}/public`, this.root);

		  complete(root, pkg.name);
	  };

exports = module.exports = function (program, f) {
	func = f;

	program
		.command("update <div>")
		.description("init root")
		.action(function (dir, opt) {
			if (!dir) throw "fail dir";

			dir = path.resolve(dir);
			if (!fs.existsSync(dir)) throw "dir not exists";

			let core = {
				root: dir
			};

			update.call(core, dir);
		});
};