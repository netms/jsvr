/**
 *
 * jBD ES6环境检测函数, 引用了es-checker库
 *
 */
(function e (t, n, r) {
	function s (o, u) {
		if (!n[o]) {
			if (!t[o]) {
				var a = typeof require == "function" && require;
				if (!u && a) return a(o, !0);
				if (i) return i(o, !0);
				var f = new Error("Cannot find module '" + o + "'");
				throw f.code = "MODULE_NOT_FOUND", f
			}
			var l = n[o] = {exports: {}};
			t[o][0].call(l.exports, function (e) {
				var n = t[o][1][e];
				return s(n ? n : e)
			}, l, l.exports, e, t, n, r)
		}
		return n[o].exports
	}

	var i = typeof require == "function" && require;
	for (var o = 0; o < r.length; o++) s(r[o]);
	return s
})({
	1:                   [function (require, module, exports) {
		var api = {
			letConst:               {passes: "'use strict'; let a; const b = 2;"},
			letLoop:                {passes: "'use strict'; for(let i in {}){}; for(let i=0;;){break}"},
			constLoop:              {passes: "'use strict'; for(const i in {}){}; for (const i=0;;){break}"},
			defaultParameter:       {passes: "'use strict'; function a(b=2){}"},
			spreadRest:             {passes: "'use strict'; var a = [1,2]; +function b(...c){}(...a);"},
			destructuring:          {passes: "'use strict'; var a = [1,2], [b,c] = a, d = {e:1,f:2}, {e:E,f} = d;"},
			parameterDestructuring: {passes: "'use strict'; function a({b,c}){}"},
			templateString:         {passes: "'use strict'; var a = 1, b = `c${a}d`;"},
			forOf:                  {passes: "'use strict'; for (var a of [1]) {}"},
			arrow:                  {passes: "'use strict'; var a = () => {};"},
			generator:              {passes: "'use strict'; function *a(){ yield; }"},
			conciseMethodProperty:  {passes: "'use strict'; var a = 1, b = { c(){}, a };"},
			computedProperty:       {passes: "'use strict'; var a = 1, b = { ['x'+a]: 2 };"},
			moduleExport:           {passes: "'use strict'; export var a = 1;"},
			moduleImport:           {passes: "'use strict'; import {a} from 'b';"},
			classes:                {passes: "'use strict'; class Foo {}; class Bar extends Foo {};"},
			numericLiteral:         {passes: "'use strict'; var a = 0o1, b = 0b10;"},
			oldOctalLiteral:        {passes: "var a = 01;"},
			symbol:                 {passes: "'use strict'; var a = Symbol('b');"},
			symbolImplicitCoercion: {dependencies: ["symbol"], fails: "'use strict'; var a = Symbol('a'); a + '';"},
			unicodeEscape:          {passes: "'use strict'; var a = '\\u{20BB7}';"},
			unicodeIdentifier:      {passes: "'use strict'; var \\u{20BB7};"},
			unicodeRegExp:          {passes: "'use strict'; var a = /\\u{20BB7}/u;"},
			stickyRegExp:           {passes: "'use strict'; var a = /b/y;"},
			letTDZ:                 {dependencies: ["letConst"], fails: "'use strict'; a = 1; let a;"},
			letLoopScope:           {
				dependencies: ["letLoop", "forOf"],
				passes:       "'use strict'; var x=[],i=0;for(let i=2;i<3;i++){x.push(function(){return i})};for(let i in {3:0}){x.push(function(){return i})};for(let i of [4]){x.push(function(){return i})};if(x[0]()*x[1]()*x[2]()!=24) throw 0;"
			},
			constRedef:             {dependencies: ["letConst"], fails: "'use strict'; const a = 1; a = 2;"},
			objectProto:            {passes: "'use strict'; var a = { b: 2 }, c = { __proto__: a }; if (c.b !== 2) throw 0;"},
			objectSuper:            {passes: "'use strict'; var a = { b: 2 }, c = { d() { return super.b; } }; Object.setPrototypeOf(c,a); if (c.d() !== 2) throw 0;"},
			extendNatives:          {
				dependencies: ["class"],
				passes:       "'use strict'; class Foo extends Array { }; var a = new Foo(); a.push(1,2,3); if (a.length !== 3) throw 0;"
			},
			TCO:                    {passes: "'use strict'; +function a(b){ if (b<6E4) a(b+1); }(0);"},
			functionNameInference:  {passes: "'use strict'; var a = { b: function(){} }; if (a.b.name != 'b') throw 0;"},
			ObjectStatics:          {is: "'use strict'; return ('getOwnPropertySymbols' in Object) && ('assign' in Object) && ('is' in Object);"},
			ArrayStatics:           {is: "'use strict'; return ('from' in Array) && ('of' in Array);"},
			ArrayMethods:           {is: "'use strict'; return ('fill' in Array.prototype) && ('find' in Array.prototype) && ('findIndex' in Array.prototype) && ('entries' in Array.prototype) && ('keys' in Array.prototype) && ('values' in Array.prototype);"},
			TypedArrays:            {is: "'use strict'; return ('ArrayBuffer' in global) && ('Int8Array' in global) && ('Uint8Array' in global) && ('Int32Array' in global) && ('Float64Array' in global);"},
			TypedArrayStatics:      {
				dependencies: ["TypedArrays"],
				is:           "'use strict'; return ('from' in Uint32Array) && ('of' in Uint32Array);"
			},
			TypedArrayMethods:      {
				dependencies: ["TypedArrays"],
				is:           "'use strict'; var x = new Int8Array(1); return ('slice' in x) && ('join' in x) && ('map' in x) && ('forEach' in x);"
			},
			StringMethods:          {is: "'use strict'; return ('includes' in String.prototype) && ('repeat' in String.prototype);"},
			NumberStatics:          {is: "'use strict'; return ('isNaN' in Number) && ('isInteger' in Number);"},
			MathStatics:            {is: "'use strict'; return ('hypot' in Math) && ('acosh' in Math) && ('imul' in Math);"},
			collections:            {is: "'use strict'; return ('Map' in global) && ('Set' in global) && ('WeakMap' in global) && ('WeakSet' in global);"},
			Proxy:                  {is: "'use strict'; return ('Proxy' in global);"},
			Promise:                {is: "'use strict'; return ('Promise' in global);"},
			Reflect:                {is: "'use strict'; return ('Reflect' in global);"},
		};

		module.exports = api;

	}, {}], 2:           [function (require, module, exports) {
		var Supports = function () {
			// Variables
			this.letConst = 'letConst';
			this.letTDZ = 'letTDZ';
			this.constRedef = 'constRedef';
			this.destructuring = 'destructuring';
			this.spreadRest = 'spreadRest';
			// Data Types
			this.forOf = 'forOf';
			this.collections = 'collections';
			this.symbol = 'symbol';
			this.Symbol = this.symbol;
			this.symbolImplicitCoercion = 'symbolImplicitCoercion';
			// Number
			this.numericLiteral = 'numericLiteral';
			this.oldOctalLiteral = 'oldOctalLiteral';
			this.MathStatics = 'MathStatics';
			this.mathStatics = this.MathStatics;
			this.NumberStatics = 'NumberStatics';
			this.numberStatics = this.NumberStatics;
			// String
			this.StringMethods = 'StringMethods';
			this.stringMethods = this.StringMethods;
			this.unicodeEscape = 'unicodeEscape';
			this.unicodeIdentifier = 'unicodeIdentifier';
			this.unicodeRegExp = 'unicodeRegExp';
			this.stickyRegExp = 'stickyRegExp';
			this.templateString = 'templateString';
			// Function
			this.arrow = 'arrow';
			this.defaultParameter = 'defaultParameter';
			this.parameterDestructuring = 'parameterDestructuring';
			this.functionNameInference = 'functionNameInference';
			this.TCO = 'TCO';
			this.tco = this.TCO;
			// Array
			this.ArrayMethods = 'ArrayMethods';
			this.arrayMethods = this.ArrayMethods;
			this.ArrayStatics = 'ArrayStatics';
			this.arrayStatics = this.ArrayStatics;
			this.TypedArrayMethods = 'TypedArrayMethods';
			this.typedArrayMethods = this.TypedArrayMethods;
			this.TypedArrayStatics = 'TypedArrayStatics';
			this.typedArrayStatics = this.TypedArrayStatics;
			this.TypedArrays = 'TypedArrays';
			this.typedArrays = this.TypedArrays;
			// Object
			this.objectProto = 'objectProto';
			this.ObjectStatics = 'ObjectStatics';
			this.objectStatics = this.ObjectStatics;
			this.computedProperty = 'computedProperty';
			this.conciseMethodProperty = 'conciseMethodProperty';
			this.Proxy = 'Proxy';
			this.proxy = this.Proxy;
			this.Reflect = 'Reflect';
			this.reflect = this.Reflect;
			// Generator and Promise
			this.generator = 'generator';
			this.Promise = 'Promise';
			this.promise = this.Promise;
			// Class
			this.classes = 'classes';
			this.class = this.classes;
			this.objectSuper = 'objectSuper';
			this.extendNatives = 'extendNatives';
			// Module
			this.moduleExport = 'moduleExport';
			this.moduleImport = 'moduleImport';
		};

		module.exports = new Supports();

	}, {}], 3:           [function (require, module, exports) {
		var api = require('./api');
		var supports = {};
		supports._api = api;

		function runTest (key) {
			if (key === 'class') key = 'classes';
			if (supports._api[key].dependencies) {
				for (var i = 0; i < supports._api[key].dependencies.length; i++) {
					var depKey = supports._api[key].dependencies[i];
					if (runTest(depKey) === false) return false;
				}
			}

			if (supports._api[key].passes) {
				return tryPassFail(supports._api[key].passes);
			}
			else if (supports._api[key].fails) {
				return !tryPassFail(supports._api[key].fails);
			}
			else if (supports._api[key].is) {
				return tryReturn(supports._api[key].is);
			}
			else if (supports._api[key].not) {
				return !tryReturn(supports._api[key].not);
			}
		}

		function tryPassFail (code) {
			try {
				runIt(code);
				return true;
			}
			catch (err) {
				return false;
			}
		}

		function tryReturn (code) {
			try {
				return runIt(code);
			}
			catch (err) {
				return false;
			}
		}

		function runIt (code) {
			return (new Function(code))();
		}

		module.exports = runTest;

	}, {"./api": 1}], 4: [function (require, module, exports) {
		var supports = require('../../lib/interface');
		var api = require('../../lib/api');
		var runTest = require('../../lib/runtest');

		global = window;
		for (var key in supports) {
			supports[key] = runTest(supports[key]);
		}

		supports._api = api;
		window.Supports = supports;

	}, {"../../lib/api": 1, "../../lib/interface": 2, "../../lib/runtest": 3}]
}, {}, [4]);

(function (checks, global, out) {
	function printError () {
		var doc  = document,
			node = doc.createElement("div");

		node.innerHTML = "游览器核心功能不支持, 请更换[Google Chrome/Firefox/360游览器/猎豹] 最新版本";
		node.style.display = "block";
		node.style.position = "fixed";
		node.style.width = "80%";
		node.style.height = "30px";
		node.style.margin = "5px 0 0 -40%";
		node.style.padding = "5px 0";
		node.style.left = "50%";
		node.style.backgroundColor = "#dd0000";
		node.style.color = "#ffffff";
		node.style.font = "bold 16px/30px 黑体";
		node.style.textAlign = "center";
		node.style.borderRadius = "10px";
		node.onclick = function () { window.open("/inc/src/chrome.exe") };

		doc.body.insertBefore(node, doc.body.firstChild);
		global.es6 = false;
	}

	function parseChecks (sup, list, out) {
		var r = {
				"Variables": [0, 0],
				"DataTypes": [0, 0],
				"Number":    [0, 0],
				"String":    [0, 0],
				"Function":  [0, 0],
				"Array":     [0, 0],
				"Object":    [0, 0],
				"Class":     [0, 0],
				"Module":    [0, 0]
			},
			t = [0, 0],
			k, v, i, d;

		try {
			for (k in list) {
				v = list[k];
				t[1] += v.length;
				r[k][1] = v.length;
				for (i = 0; i < v.length; i++) {
					d = v[i];
					if (d.state = sup[d.key]) {
						r[k][0]++;
						t[0]++;
					}
					else {
						console.error("[ES-Checker]", d.value, "无效!");
						if (!d.level) throw 0;
					}
				}
			}
		}
		catch (e) {
			printError();
		}

		if (out) {
			console.info("=========================");
			console.info("[ES-Checker] Total:", parseInt(t[0] * 100 / t[1]) + "%");
			for (k in r) console.info("[ES-Checker]", k, ":", parseInt(r[k][0] * 100 / r[k][1]) + "%");
		}
	}

	global.es6 = true;
	global.onload = function () {
		if (Supports) parseChecks(Supports, checks, out);
		else {
			printError();
			console.error("[ES-Checker] init error, msg: Supports is undefined");
		}
	}
})({
	"Variables": [
		{level: 0, key: "letConst", value: "let 和 const 命令"},
		{level: 1, key: "letTDZ", value: "let命令的暂时性死区"},
		{level: 0, key: "constRedef", value: "不允许再次用const声明同一变量"},
		{level: 1, key: "destructuring", value: "解构赋值"},
		{level: 1, key: "spreadRest", value: "扩展（...）运算符"}
	],
	"DataTypes": [
		{level: 0, key: "forOf", value: "for...of循环"},
		{level: 0, key: "collections", value: "Map, Set, WeakMap, WeakSet"},
		{level: 0, key: "symbol", value: "Symbol类型"},
		{level: 1, key: "symbolImplicitCoercion", value: "Symbol值不能用于运算"}
	],
	"Number":    [
		{level: 1, key: "numericLiteral", value: "数值的八进制和二进制表示法"},
		{level: 1, key: "oldOctalLiteral", value: "不再支持八进制的前缀零表示法"},
		{level: 1, key: "mathStatics", value: "Math对象的静态方法"},
		{level: 1, key: "numberStatics", value: "Number对象的静态方法"}
	],
	"String":    [
		{level: 1, key: "stringMethods", value: "字符串的实例方法"},
		{level: 1, key: "unicodeEscape", value: "Unicode字符的大括号表示法"},
		{level: 1, key: "unicodeIdentifier", value: "Unicode字符是否可用作标识名"},
		{level: 1, key: "unicodeRegExp", value: "正则表达式的u修饰符"},
		{level: 1, key: "stickyRegExp", value: "正则表达式的y修饰符"},
		{level: 0, key: "templateString", value: "模板字符串"}
	],
	"Function":  [
		{level: 0, key: "arrow", value: "箭头函数"},
		{level: 1, key: "defaultParameter", value: "函数的默认参数"},
		{level: 1, key: "parameterDestructuring", value: "函数参数的解构"},
		{level: 1, key: "functionNameInference", value: "匿名函数的name属性推断函数名"},
		{level: 1, key: "tco", value: "尾调用优化"}
	],
	"Array":     [
		{level: 0, key: "arrayStatics", value: "数组的静态方法"},
		{level: 1, key: "arrayMethods", value: "数组的实例方法"},
		{level: 0, key: "typedArrays", value: "类型化数组"},
		{level: 1, key: "typedArrayStatics", value: "类型化数组的静态方法"},
		{level: 1, key: "typedArrayMethods", value: "类型化数组的实例方法"}
	],
	"Object":    [
		{level: 1, key: "objectProto", value: "对象的proto属性"},
		{level: 0, key: "objectStatics", value: "Object的静态方法"},
		{level: 1, key: "computedProperty", value: "对象属性名使用表达式"},
		{level: 1, key: "conciseMethodProperty", value: "对象属性的简洁表示法"},
		{level: 1, key: "proxy", value: "Proxy对象"},
		{level: 1, key: "generator", value: "Generator函数"},
		{level: 0, key: "promise", value: "Promise对象"}
	],
	"Class":     [
		{level: 1, key: "class", value: "类（class）"},
		{level: 1, key: "objectSuper", value: "对象方法是否可以使用super"},
		{level: 1, key: "extendNatives", value: "原生类型的扩展"}
	],
	"Module":    [
		{level: 1, key: "moduleExport", value: "模块的export命令"},
		{level: 1, key: "moduleImport", value: "模板的import命令"}
	]
}, window, false);