const path = require("path"),
	  jBD  = require("jbd"),
	  jSvr = require("jsvr");

{module1}

let cfg, core, app;

cfg = jSvr.Config(
	path.join(__dirname, "./config.json"),
	{
		api:    path.join(__dirname, "./api"),
		upload: path.join(__dirname, "./upload"),
		root: path.join(__dirname, "./public"),
		page:   path.join(__dirname, "./page"),
		view:   path.join(__dirname, "./view")
	},
	{
{plugins1}
	},
	(cfg, key, svr, file, opt, plugins) => {
		switch (key) {

		}
	}
);

jSvr.Depend(path.join(__dirname, "./package.json"), cfg);

core = {module2}(cfg.plugins, cfg.module);

app = jSvr(cfg, core, app => {
	{css}
	app.use(require("koa2-compass")({
		mode:    "compress",
		project: path.join(__dirname),
		sass:    "./scss",
		css:     "./public/css"
	}));
});