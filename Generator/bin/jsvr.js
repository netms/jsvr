#!/usr/bin/env node

const program  = require("commander"),
	  os       = require("os"),
	  fs       = require("fs"),
	  path     = require("path"),
	  readline = require("readline"),
	  et       = process.exit,
	  cfg      = require("../package.json");

const exit    = function (code) {
		  var draining = 0,
			  streams  = [process.stdout, process.stderr],
			  done     = function () {
				  if (!(draining--)) et(code);
			  };

		  exit.exited = true;

		  streams.forEach(function (stream) {
			  draining += 1;
			  stream.write("", done);
		  });

		  done();
	  },
	  before  = function (obj, method, fn) {
		  let old = obj[method];

		  obj[method] = function () {
			  fn.call(this);
			  old.apply(this, arguments);
		  };
	  },
	  confirm = function (msg, callback) {
		  let rl = readline.createInterface({
			  input:  process.stdin,
			  output: process.stdout
		  });

		  rl.question(msg, function (input) {
			  rl.close();
			  callback(/^y|yes|ok|true$/i.test(input));
		  });
	  },
	  empty   = function (path, fn) {
		  fs.readdir(path, function (err, files) {
			  if (err && "ENOENT" != err.code) throw err;
			  fn(!files || !files.length);
		  });
	  },
	  mkdir   = function (root, refer) {
		  if (!fs.existsSync(root)) fs.mkdirSync(root);

		  console.log("	\033[36mcreate\033[0m : ./" + path.relative(refer, root));
	  },
	  cpdir   = function (src, dest, refer) {
		  const list = (src, callback) => {
					let l     = fs.readdirSync(src),
						state = true,
						count = 0;

					for (let i = 0, o; i < l.length; i++) {
						if (!state) return;

						count++;
						o = path.join(src, l[i]);

						if (callback(l[i], o, fs.statSync(o)) === false) break;
					}
				},
				file = (src, dest) => {
					fs.writeFileSync(dest, fs.readFileSync(src));
					console.log("	\x1b[36mcopy\x1b[0m : ./" + path.relative(refer, dest));
				},
				dir  = (src, dest) => {
					if (!fs.existsSync(dest)) fs.mkdirSync(dest);

					list(src, (f, p, o) => {
						if (o.isDirectory()) dir(path.join(src, f), path.join(dest, f));
						else file(path.join(src, f), path.join(dest, f));
					});
				};

		  src = path.join(__dirname, "../template", program.type || "koa", src);

		  dir(src, dest);
	  },
	  write   = function (root, str, refer) {
		  fs.writeFileSync(root, str, {mode: 0666});
		  console.log("	\x1b[36mcreate\x1b[0m : ./" + path.relative(refer, root));
	  },
	  launch  = function () {
		  return process.platform === "win32" && process.env._ === undefined;
	  },
	  load    = function (name) {
		  return fs.readFileSync(path.join(__dirname, "../template", program.type || "koa", name), "utf-8");
	  };

const eol  = os.EOL,
	  func = {exit, before, confirm, empty, mkdir, cpdir, write, launch, load};

process.exit = exit;

before(program, "outputHelp", function () {
	this.allowUnknownOption();
});

program
	.version(cfg.version, "-v, --version")
	.usage("<cmd> <dir>");

require("../lib/init.js")(program, func);
require("../lib/update.js")(program, func);

program.parse(process.argv);

if (program.args.length < 2) program.help();
if (exit.exited) return;