"use strict";

module.exports = exports = async function (global, info, code, out) {
	const dtd  = jBD.Deferred(true),
		  path = require("path"),
		  cfg  = require(path.join(code, "config.json"));

	global.delDir(out, false);

	console.log("─┬─ Generator@" + cfg.info.version);
	cfg.pack = global.deep(cfg.info);
	cfg.pack = global.deep(info, cfg.pack);
	global.each(cfg.core, (d, k) => {
		global.copyDir(path.join(code, d), path.join(out, d));
		console.log(" ├─ /" + d);
	});
	global.copyFile(path.join(out, "../CSS/global.min.css"), path.join(out, "template/public/inc/src/global.min.css"));
	console.log(" ├─ global.min.css");
	global.save(path.join(out, "package.json"), cfg.pack);
	// global.copyFile(path.join(cache, "jSvr.js"), path.join(out, "jSvr.js"));
	console.log(" └─ done\n");

	dtd.resolve();

	return dtd.promise();
};