"use strict";

module.exports = exports = async function (global, info, code, out) {
	const dtd   = jBD.Deferred(true),
		  path  = require("path"),
		  cfg   = require(path.join(code, "config.json")),
		  cache = path.join(out, "../cache");

	const init_jSvr  = (core, loading, fn) => {
			  fn = path.join(code, "node_jSvr.js");
			  let c = "",
				  f = "";

			  for (let i = 0; i < core.length; i++) {
				  c += global.readFile(path.join(code, core[i])) + "\n";
			  }

			  for (let i = 0; i < loading.length; i++) {
				  f += global.readFile(path.join(code, loading[i])) + "\n";
			  }

			  let fw = global.readFile(path.join(code, "jSvr.js"));

			  c = fw.replace("\"@jSvr_Code\";", c);
			  c = c.replace("@version", cfg.info.version);
			  c = c.replace("\"@jSvr_Loading\";", f);

			  global.writeFile(fn, c);
		  },
		  init_Cache = (code, out, cache) => {
			  let list = {};

			  console.log("─┬─ cache");

			  console.log(" ├─ jSvr.js");
			  init_jSvr(cfg.core, cfg.loading);
			  global.miniFile(path.join(code, "node_jSvr.js"), path.join(cache, "jSvr.js"));

			  console.log(" ├┬ lib");
			  global.each(cfg.lib, (d, k, l) => {
				  l = "lib/" + d;

				  console.log(" │├─ " + d);
				  list[l] = l;
				  global.miniFile(path.join(code, l), path.join(cache, l));
			  });
			  console.log(" │└─ done\n │");

			  console.log(" ├┬ api");
			  global.each(cfg.api, (d, k, l) => {
				  l = "api/" + d;

				  console.log(" │├─ " + d);
				  list[l] = l;
				  global.miniFile(path.join(code, l), path.join(cache, l));
			  });
			  console.log(" │└─ done\n │");

			  // console.log(" ├┬ src");
			  // global.each(cfg.src.files, (d, o) => {
			  // 	o = path.join(code, d);
			  // 	d = path.basename(o);
			  // 	list.web["src/" + d] = "./src/" + d;
			  // 	global.miniFile(o, path.join(cache, "src", d));
			  // 	console.log(" │├─ " + o);
			  // });
			  // console.log(" │└─ done\n │");

			  console.log(" └─ done\n");

			  return list;
		  };

	global.delDir(out, false);
	//global.delFile(path.join(code, "jBD.js"));

	cfg.cache = init_Cache(code, out, cache);

	console.log("─┬─ jSvr@" + cfg.info.version);
	cfg.pack = global.deep(cfg.info);
	cfg.pack = global.deep(info, cfg.pack);
	cfg.pack.main = "jSvr.js";
	cfg.pack.files = [];
	global.each(cfg.cache, (d, k) => {
		cfg.pack.files.push(k);
		global.copyFile(path.join(cache, d), path.join(out, d));
		console.log(" ├─ " + d);
	});
	global.save(path.join(out, "package.json"), cfg.pack);
	global.copyFile(path.join(cache, "jSvr.js"), path.join(out, "jSvr.js"));
	console.log(" └─ done\n");

	global.delDir(cache, true);
	global.delFile(path.join(code, "node_jSvr.js"));

	dtd.resolve();

	return dtd.promise();
};