"use strict";

const os       = require("os"),
	  path     = require("path"),
	  fs       = require("fs"),
	  spawn    = require("child_process").spawn,
	  cleanCSS = require("clean-css");

async function compass (root, input, output) {
	let dtd     = jBD.Deferred(true),
		exec    = "compass",
		options = [
			"compile",
			"--no-line-comments",
			"--relative-assets"
		],
		obj;

	// options.push("--output-style", "compress");
	options.push("--output-style", "compressed");
	options.push("--css-dir", output);
	options.push("--sass-dir", input);

	if (os.platform() === "win32") exec += ".bat";

	obj = spawn(
		exec, options,
		{cwd: root}
	);

	obj.on("error", function (err) {
		dtd.reject(err);
	});
	obj.on("exit", function (code) {
		dtd.resolve();
	});

	return dtd.promise();
}

async function minify (cfg, input, output) {
	input = Array.isArray(input) ? input : [input];

	let result = fs.readFileSync(input[0], "utf8");

	result = result.replace("{name}", cfg.info.name)
		.replace("{author}", cfg.author.name)
		.replace("{mail}", cfg.author.email)
		.replace("{license}", cfg.info.license)
		.replace("{version}", cfg.version);

	for (let i = 1, d; i < input.length; i++) {
		d = await cleanCSS.process(fs.readFileSync(input[i], "utf8"), {});
		result += d.css;
	}

	fs.writeFileSync(output, result, "utf8");
}

module.exports = exports = async function (global, info, code, out) {
	let dtd = jBD.Deferred(true),
		cfg = require(path.join(code, "config.json"));

	global.delDir(out, false);
	global.makeDir(out + "/");

	console.log("─┬─ CSS@" + cfg.version);
	await compass(code, "./scss", "./.cache");
	console.log(" ├─ /scss");
	await minify(
		{
			info:    cfg.info,
			author:  info.author,
			version: cfg.version
		},
		[
			path.join(code, "./css/global.css"),
			path.join(code, "./css/normalize.css"),
			path.join(code, "./.cache/style.css")
		],
		path.join(out, "global.min.css")
	);
	console.log(" ├─ global.min.css");

	console.log(" └─ done\n");

	global.delDir(path.join(code, "./.sass-cache"), true);
	global.delDir(path.join(code, "./.cache"), true);

	dtd.resolve();

	return dtd.promise();
};